package kr.co.niceday.common.engine.test;

import kr.co.niceday.common.engine.helper.path.PathHelper;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

/**   
 * @since       2018.10.03
 * @author      lucas
 * @description test helper
 **********************************************************************************************************************/
public class TestHelper {

	public static MockMvc mock;
	public static RestDocumentationResultHandler handler;

	public static void initialize(MockMvc _mock, RestDocumentationResultHandler _handler){
		mock    = _mock;
		handler = _handler;
	}

	public static MockHttpServletRequestBuilder get(String url){
		return RestDocumentationRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON);
	}

	public static MockHttpServletRequestBuilder get(String url, Object ... params){
		return RestDocumentationRequestBuilders.get(url, params).contentType(MediaType.APPLICATION_JSON);
	}

	public static MockHttpServletRequestBuilder post(String url){
		return RestDocumentationRequestBuilders.post(url).contentType(MediaType.APPLICATION_JSON);
	}

	public static MockHttpServletRequestBuilder post(String url, Object ... params){
		return RestDocumentationRequestBuilders.post(url, params).contentType(MediaType.APPLICATION_JSON);
	}

	public static MockHttpServletRequestBuilder put(String url, Object ... params){
		return RestDocumentationRequestBuilders.put(url, params).contentType(MediaType.APPLICATION_JSON);
	}

	public static MockHttpServletRequestBuilder delete(String url, Object ... params){
		return RestDocumentationRequestBuilders.delete(url, params).contentType(MediaType.APPLICATION_JSON);
	}

	@SneakyThrows
	public static MockHttpServletRequestBuilder file(String url, String filePath, Object ... params) {
		return RestDocumentationRequestBuilders
				.fileUpload(url, params)
				.file(new MockMultipartFile("file", PathHelper.getPath(filePath).getName(), MediaType.MULTIPART_FORM_DATA_VALUE, FileUtils.openInputStream(PathHelper.getPath(filePath))))
				.contentType(MediaType.MULTIPART_FORM_DATA_VALUE);
	}
}