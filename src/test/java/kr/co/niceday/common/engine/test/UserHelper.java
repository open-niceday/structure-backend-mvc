package kr.co.niceday.common.engine.test;

import com.google.common.collect.ImmutableList;
import kr.co.niceday.cms.access.account.entity.Account;
import kr.co.niceday.cms.access.role.entity.Role;
import kr.co.niceday.cms.access.role.enumerate.RoleType;
import kr.co.niceday.common.engine.constant.Constant;
import kr.co.niceday.common.engine.helper.account.Principal;
import org.assertj.core.util.Lists;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**   
 * @since       2021.12.21
 * @author      preah
 * @description user helper
 **********************************************************************************************************************/
public class UserHelper {

	public static Account account;

	public static void initialize(){
		account = Account.builder()
				.id(1L)
				.name("관리자")
				.identifier("admin")
				.roles(Lists.newArrayList(Role.builder().roleType(RoleType.ROLE_ADMIN).build()))
				.email("adminh@niceday.io")
				.build();

		SecurityContextHolder.getContext().setAuthentication(
				new UsernamePasswordAuthenticationToken(new Principal(account), Constant.String.EMPTY, ImmutableList.of(getGrantedAuthority(RoleType.ROLE_ADMIN), getGrantedAuthority(RoleType.ROLE_USER), getGrantedAuthority(RoleType.ROLE_ANONYMOUS)))
		);
	}

	private static GrantedAuthority getGrantedAuthority(RoleType roleType){
		return new SimpleGrantedAuthority(roleType.name());
	}
}