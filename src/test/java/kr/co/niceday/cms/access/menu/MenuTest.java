package kr.co.niceday.cms.access.menu;

import kr.co.niceday.cms.access.account.mapper.MenuContext;
import kr.co.niceday.cms.access.menu.entity.Menu;
import kr.co.niceday.cms.access.menu.exception.MenuException;
import kr.co.niceday.cms.access.menu.form.MenuForm.Request;
import kr.co.niceday.cms.access.menu.form.MenuForm.Response;
import kr.co.niceday.cms.access.menu.repository.MenuRepository;
import kr.co.niceday.cms.access.menu.service.MenuService;
import kr.co.niceday.cms.access.role.entity.Role;
import kr.co.niceday.cms.access.role.enumerate.RoleType;
import kr.co.niceday.cms.access.role.repository.RoleRepository;
import kr.co.niceday.common.engine.exception.NoFoundException;
import kr.co.niceday.common.engine.exception.common.ExceptionCode;
import kr.co.niceday.common.engine.test.SuperTest;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Hibernate;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.CastUtils;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.IntStream;

import static kr.co.niceday.cms.access.menu.mapper.MenuMapper.mapper;
import static kr.co.niceday.common.engine.helper.model.ObjectHelper.newInstance;

/**
 * @since       2021.12.21
 * @author      preah
 * @description menu test
 **********************************************************************************************************************/
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional
@Slf4j
public class MenuTest extends SuperTest {


    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private MenuService menuService;

    @Test
    public void t01_tree() {

        Menu root = menuRepository.findByParent(null)
                                  .orElseThrow(() -> new NoFoundException(ExceptionCode.E00100002));



        List<Menu> rootNodes =
            Arrays.asList(
                      Menu.builder().parent(root).name("시스템관리").sort(1).useYn(true).url("/").build(),
                      Menu.builder().parent(root).name("샘플")      .sort(2).useYn(true).url("/sample/page").build());

        root.setChildren(rootNodes);
        menuRepository.saveAll(rootNodes);

        Menu system = menuRepository.findAll().stream()
                                              .filter(menu -> menu.getName().equals("시스템관리")).findAny()
                                              .orElse(null);
        List<Menu> systemNodes =
                Arrays.asList(
                        Menu.builder().parent(system).name("사용자").sort(1).useYn(true).url("/access/account/user/page").build(),
                        Menu.builder().parent(system).name("권한")  .sort(2).useYn(true).url("/access/role/page").build(),
                        Menu.builder().parent(system).name("자원")  .sort(3).useYn(true).url("/access/resource/page").build());

        system.setChildren(systemNodes);

        menuRepository.saveAll(systemNodes);
    }

    @Test
    public void t02_role() {
        List<Role> roles = roleRepository.findAll();
        List<Menu> menus = menuRepository.findAll();

        for(Role role : roles) {
            role.setMenus(menus);
        }

        roleRepository.saveAll(roles);
    }

    @Test
    public void t03_getRoot() {
        Menu root = menuService.getRoot();

        Response.FindOne findOne = mapper.toFindOne(root);
        log.info(findOne.toString());
    }


    @Test
    public void t04_add() {
        Request.Add add = newInstance(Request.Add.class);
        add.setParent(Request.Add.Parent.builder().id(1L).build());
        menuService.add(mapper.toMenu(add));
    }

    @Test
    public void t05_modify() {

        Request.Add addForm = newInstance(Request.Add.class);
        addForm.setParent(Request.Add.Parent.builder().id(1L).build());
        Menu add = menuService.add(mapper.toMenu(addForm));

        Request.Modify modifyForm = newInstance(Request.Modify.class).toBuilder().id(add.getId()).build();
        menuService.modify(mapper.toMenu(modifyForm));
    }

    @Test(expected=MenuException.class)
    public void t06_remove() {
        menuService.remove(1L);
    }

    @Test
    public void t07_getRoleMenu() {

        Menu root = CastUtils.cast(Hibernate.unproxy(menuRepository.getOne(1L)));
        HashSet roleTypes = new HashSet<>();
        roleTypes.addAll(Arrays.asList(RoleType.ROLE_USER, RoleType.ROLE_ADMIN));
        List<Menu> menus = menuRepository.findByUseYnAndRolesRoleTypeIn(Boolean.TRUE, roleTypes);
//        Menu node = mapper.toFilter(root, new MenuContext(menus));

//        log.debug(node.toString());
    }

    @Test
    public void t08_getNode() {
        IntStream.range(0, 101).forEach(idx -> {
            log.info(String.valueOf(idx));
//            menuService.getNode(PrincipalHelper.getAccount().getRoles().stream()
//                                                                        .map(Role::getRoleType)
//                                                                        .collect(Collectors.toSet()));
        });
    }
}