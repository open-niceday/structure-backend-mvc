package kr.co.niceday.cms.sample;

import kr.co.niceday.common.engine.test.SuperTest;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.springframework.transaction.annotation.Transactional;

/**
 * @since       2021.03.03
 * @author      minam
 * @description sample test
 **********************************************************************************************************************/
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional
public class SampleTest extends SuperTest {

//	@Test
//	public void t01_getPage() {
//		SampleHelper.getPage(Request.Find.builder().build());
//	}
//
//	@Test
//	public void t02_get() {
//		SchoolHelper.get(SchoolHelper.add(SchoolHelper.addSchool()));
//	}
//
//	@Test
//	public void t03_add() {
//		SchoolHelper.add(SchoolHelper.addSchool());
//	}
//
//	@Test
//	public void t04_modify() {
//		SchoolHelper.modify(SchoolHelper.add(SchoolHelper.addSchool()), SchoolHelper.modifySchool());
//	}
//
//	@Test
//	public void t05_remove() {
//		SchoolHelper.remove(SchoolHelper.add(SchoolHelper.addSchool()));
//	}
//
//	@Test
//	public void t06_listViaAjax() {
//		SampleHelper.listViaAjax();
//	}
//
//	@Test
//	public void t07_addHtml() {
//		SampleHelper.addHtml();
//	}
//
//	@Test
//	public void t08_detailHtml() {
//		SampleHelper.detailHtml(Request.Find.builder().id(SchoolHelper.add(SchoolHelper.addSchool())).build());
//	}
//
//	@Test
//	public void t09_modifyHtml() {
//		SampleHelper.modifyHtml(Request.Find.builder().id(SchoolHelper.add(SchoolHelper.addSchool())).build());
//	}
//
//	@Test
//	public void t10_post() {
//		SampleHelper.post(SampleHelper.createTestSchool());
//	}
//
//	@Test
//	public void t11_modify() {
//		SampleHelper.modify(
//				Request.Modify.builder().id(SampleHelper.post(SampleHelper.createTestSchool()))
//						.creator("MODIFIER")
//						.name("MODIFIER")
//						.contents("MODIFIED CONTENT")
//						.build()
//		);
//	}
//
//	@Test
//	public void t12_delete() {
//		SampleHelper.delete(SampleHelper.post(SampleHelper.createTestSchool()));
//	}
//
//	@Test
//	public void t13_getSamples() {
//		Pageable datatablePageable = Pageable.builder().size(10).number(1).build();
//
//		SampleHelper.getSamples(Request.Find.builder().build(), datatablePageable);
//	}
}