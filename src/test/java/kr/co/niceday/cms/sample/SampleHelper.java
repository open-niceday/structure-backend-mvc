package kr.co.niceday.cms.sample;

import kr.co.niceday.common.engine.test.TestHelper;
import lombok.extern.slf4j.Slf4j;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

/**
 * @since       2021.03.03
 * @author      minam
 * @description sample helper
 **********************************************************************************************************************/
@Slf4j
public class SampleHelper extends TestHelper {

//    private static final String SAMPLE_CONTROLLER_PREFIX = "/sample";
//
//    @SneakyThrows
//    public static void getPage(Request.Find find) {
//        mock.perform(get  (SAMPLE_CONTROLLER_PREFIX + "/page.html") )
//                .andExpect(status().isOk())
//                .andDo    (print());
//    }
//
//    @SneakyThrows
//    public static void getSamples(Request.Find find, Pageable pageable) {
//        mock.perform(get (SAMPLE_CONTROLLER_PREFIX + "/samples")
//                .contentType(MediaType.APPLICATION_JSON)
//                .params     (ObjectHelper.newMultiValueMap(find))
//                .params     (ObjectHelper.newMultiValueMap(pageable))
//        ).andExpect     (status().isOk())
//                .andDo         (print());
//
//
//    }
//
//    @SneakyThrows
//    public static void listViaAjax() {
//        mock.perform(get(SAMPLE_CONTROLLER_PREFIX + "/list-via-ajax.html")
//                .contentType(MediaType.APPLICATION_JSON)
//        ).andExpect (status().isOk())
//                .andDo     (print());
//    }
//
//    @SneakyThrows
//    public static void addHtml() {
//        mock.perform(get(SAMPLE_CONTROLLER_PREFIX + "/add.html")
//                .contentType(MediaType.TEXT_HTML)
//        ).andExpect (status().isOk())
//                .andDo     (print());
//    }
//
//    @SneakyThrows
//    public static void detailHtml(Request.Find find) {
//        mock.perform(get (SAMPLE_CONTROLLER_PREFIX + "/detail.html")
//                .contentType(MediaType.TEXT_HTML)
//                .param("id", String.valueOf(find.getId()))
//        ).andExpect (status().isOk())
//                .andDo     (print());
//    }
//
//    @SneakyThrows
//    public static void modifyHtml(Request.Find find) {
//        mock.perform(get(SAMPLE_CONTROLLER_PREFIX + "/modify.html")
//                .contentType(MediaType.TEXT_HTML)
//                .param("id", String.valueOf(find.getId()))
//        ).andExpect (status().isOk())
//                .andDo     (print());
//    }
//
//    @SneakyThrows
//    public static void templateEngineSampleHtml(Request.Find find) {
//        mock.perform(get(SAMPLE_CONTROLLER_PREFIX + "/template-engine-sample.html")
//                .contentType(MediaType.TEXT_HTML)
//                .param("id", String.valueOf(find.getId()))
//        ).andExpect (status().isOk())
//                .andDo     (print());
//    }
//
//    @SneakyThrows
//    public static Integer post(Request.Add add) {
//        MvcResult mvcResult = mock.perform(post(SAMPLE_CONTROLLER_PREFIX + "/post")
//                .flashAttr("schoolForm", add)
//        ).andExpect(status().is3xxRedirection())
//                .andDo(print()).andReturn();
//
//        return Integer.parseInt((String) Objects.requireNonNull(mvcResult.getModelAndView()).getModel().get("resultId"));
//
//    }
//
//    @SneakyThrows
//    public static void modify(Request.Modify modify) {
//        mock.perform( post(SAMPLE_CONTROLLER_PREFIX + "/modify")
//                .flashAttr("schoolDetail", modify)
//        ).andExpect(status().is3xxRedirection())
//                .andDo(print());
//    }
//
//    @SneakyThrows
//    public static void delete(Integer id) {
//        mock.perform( post(SAMPLE_CONTROLLER_PREFIX + "/delete")
//                .param("id", String.valueOf(id))
//        ).andExpect(status().is3xxRedirection())
//                .andDo(print());
//    }
//
//    public static Request.Add createTestSchool() {
//        return Request.Add.builder().name("TEST_SCHOOL").creator("TESTER").contents("<h1>HI TEST</hi>").build();
//    }
}
