package kr.co.niceday.cms.sample;

import kr.co.niceday.common.engine.test.TestHelper;
import kr.co.niceday.cms.sample.form.SchoolForm.Request;
import lombok.SneakyThrows;

import static kr.co.niceday.common.engine.helper.model.ObjectHelper.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @since       2020.02.14
 * @author      preah
 * @description school helper
 **********************************************************************************************************************/
public class SchoolHelper extends TestHelper {

    @SneakyThrows
    public static void getPage(Request.Find find) {
        mock.perform(get  ("/api/schools/pages")
                .content  (toJson(find)))
                .andExpect(status().isOk())
                .andDo    (print());
    }

    @SneakyThrows
    public static void get(Integer schoolId) {
        mock.perform(get  ("/api/schools/{schoolId}", schoolId))
                .andExpect(status().isOk())
                .andDo    (print());
    }

    @SneakyThrows
    public static Integer add(Request.Add add) {
        return toInstance(Integer.class, mock.perform(post ("/api/schools")
                .content  (toJson(add)))
                .andExpect(status().isOk())
                .andDo    (print()));
    }

    @SneakyThrows
    public static Integer modify(Integer schoolId, Request.Modify modify) {
        return toInstance(Integer.class, mock.perform(put  ("/api/schools/{schoolId}", schoolId)
                .content  (toJson(modify)))
                .andExpect(status().isOk())
                .andDo    (print()));
    }


    @SneakyThrows
    public static void remove(Integer schoolId) {
        mock.perform(delete("/api/schools/{schoolId}", schoolId))
                .andExpect (status().isOk())
                .andDo     (print());
    }

    public static Request.Find findSchool() {
        return newInstance(Request.Find.class);
    }

    public static Request.Add addSchool() {
        return newInstance(Request.Add.class);
    }

    public static Request.Modify modifySchool() {
        return newInstance(Request.Modify.class);
    }
}
