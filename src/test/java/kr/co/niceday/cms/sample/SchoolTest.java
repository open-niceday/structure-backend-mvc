package kr.co.niceday.cms.sample;

import kr.co.niceday.common.engine.test.SuperTest;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.transaction.annotation.Transactional;

/**
 * @since       2020.02.14
 * @author      preah
 * @description school test
 **********************************************************************************************************************/
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional
public class SchoolTest extends SuperTest {

	@Test
	public void t01_getPage() {
		SchoolHelper.add    (SchoolHelper.addSchool());
		SchoolHelper.add    (SchoolHelper.addSchool());
		SchoolHelper.getPage(SchoolHelper.findSchool());
	}

	@Test
	public void t02_get() {
		SchoolHelper.get(SchoolHelper.add(SchoolHelper.addSchool()));
	}

	@Test
	public void t03_add() {
		SchoolHelper.add(SchoolHelper.addSchool());
	}

	@Test
	public void t04_modify() {
		SchoolHelper.modify(SchoolHelper.add(SchoolHelper.addSchool()), SchoolHelper.modifySchool());
	}

	@Test
	public void t05_remove() {
		SchoolHelper.remove(SchoolHelper.add(SchoolHelper.addSchool()));
	}

}