/**
 * @since       2022.01.05
 * @author      preah
 * @description search tree
 **********************************************************************************************************************/
const FN = function () {

    class Menu {
        constructor(id, name, sort, url) {
            this.id   = parseInt(id);
            this.name = name;
            this.sort = parseInt(sort);
            this.url  = url;
        }
    }

    let listener;
    return {
        initialize: () => {
            listener = ModalListener.Builder
                                    .receiver(FN.onReceiver)
                                    .build();
        },
        onReceiver : (event) => {

            const exist = event.data;
            let checked = $("input:checkbox[name='menus']:checked")
                                  .filter((idx, el) => exist.includes(parseInt($(el).val())) == false)
                                  .map((idx, el) => {
                                      let index = $("input:checkbox[name='menus']").index(el);
                                      return new Menu($(el).val()
                                               , $("input:hidden[name='name']").eq(index).val()
                                               , $("input:hidden[name='sort']").eq(index).val()
                                               , $("input:hidden[name='url']") .eq(index).val())
                                  }).get();
            listener.send(checked);
        }
    }
}();

$(function(){
    $(document).ready(function(){
        FN.initialize();
    });
});