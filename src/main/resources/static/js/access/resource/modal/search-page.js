/**
 * @since       2022.01.05
 * @author      preah
 * @description search page
 **********************************************************************************************************************/
const FN = function () {

    class Resource {
        constructor(id, name, pattern) {
            this.id      = parseInt(id);
            this.name    = name;
            this.pattern = pattern;
        }
    }

    let listener;
    return {
        initialize: () => {
            listener = ModalListener.Builder
                                    .receiver(FN.onReceiver)
                                    .build();
        },
        onReceiver :(event) => {

            const exist = event.data;
            let checked = $("input:checkbox[name='resources']:checked")
                                  .filter((idx, el) => exist.includes(parseInt($(el).val())) == false)
                                  .map((idx, el) => {
                                      let index = $("input:checkbox[name='resources']:checked").index(el);
                                      return new Resource($(el).val()
                                                        , $("td[name='name']").eq(index).text()
                                                        , $("td[name='pattern']").eq(index).text())
                                  }).get();
            listener.send(checked);
        }
    }
}();

$(function(){
    $(document).ready(function(){
        FN.initialize();
    });
});