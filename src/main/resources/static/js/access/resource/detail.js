/**
 * @since       2021.12.13
 * @author      preah
 * @description resource detail
 **********************************************************************************************************************/
const FN = function () {
    return {
        initialize: function () {
        }
    }
}();

$(function(){
    $(document).ready(function(){
        FN.initialize();
    });
});