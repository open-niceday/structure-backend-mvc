/**
 * @since       2021.12.13
 * @author      preah
 * @description role modify
 **********************************************************************************************************************/
const FN = function () {
    return {
        initialize: function () {

            $("#chooseResourceBtn").on("click", FN.chooseResource);
            $("#chooseMenuBtn")    .on("click", FN.chooseMenu);
        },
        chooseResource : function () {

            const channel = ModalChannel.Builder
                                        .elementId("search-resource")
                                        .receiver(FN.addResource)
                                        .build();

            let exist = $("input:hidden[name^='resources['][name$='].id']").map(function(idx, el) {
                return parseInt($(el).val());
            }).get();

            channel.send(exist);
        },
        addResource : function (event) {

            $($.templates("#addResource").render(event.data)).appendTo("#tbody-resource");
            $("input:hidden[name^='resources['][name$='].id']").each(function(idx, el) {
                $(el).attr("name", "resources".concat("[", idx, "].id"));
            });

            $("#resourceModal").modal("hide");
        },
        chooseMenu : function () {

            const channel = ModalChannel.Builder
                                        .elementId("search-menu")
                                        .receiver(FN.addMenu)
                                        .build();

            let exist = $("input:hidden[name^='menus['][name$='].id']").map(function(idx, el) {
                return parseInt($(el).val());
            }).get();

            channel.send(exist);
        },
        addMenu : function (event) {

            $($.templates("#addMenu").render(event.data)).appendTo("#tbody-menu");
            $("input:hidden[name^='menus['][name$='].id']").each(function(idx, el) {
                $(el).attr("name", "menus".concat("[", idx, "].id"));
            });

            $("#menuModal").modal("hide");
        },
        removeRow : function (el) {
            $(el).parent().parent().remove();
        }
    }
}();

$(function(){
    $(document).ready(function(){
        FN.initialize();
    });
});