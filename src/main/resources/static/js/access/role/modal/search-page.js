/**
 * @since       2022.01.05
 * @author      preah
 * @description search page
 **********************************************************************************************************************/
const FN = function () {

    class Role {
        constructor(id, roleTypeDescription, description) {
            this.id                  = parseInt(id);
            this.roleTypeDescription = roleTypeDescription;
            this.description         = description;
        }
    }

    let listener;
    return {
        initialize: () => {
            listener = ModalListener.Builder
                                    .receiver(FN.onReceiver)
                                    .build();
        },
        onReceiver : (event) => {

            const exist = event.data;
            let checked = $("input:checkbox[name='roles']:checked")
                                  .filter((idx, el) => exist.includes(parseInt($(el).val())) == false)
                                  .map((idx, el) => {
                                      let index = $("input:checkbox[name='roles']").index(el);
                                      return new Role($(el).val()
                                                    , $("td[name='roleType.description']").eq(index).text()
                                                    , $("td[name='description']").eq(index).text());
                                  }).get();
            listener.send(checked);
        }
    }
}();

$(function(){
    $(document).ready(function(){
        FN.initialize();
    });
});