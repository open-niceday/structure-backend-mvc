/**
 * @since       2021.12.13
 * @author      preah
 * @description user add
 **********************************************************************************************************************/
const FN = function () {
    return {
        initialize: () => {
            $("#chooseRoleBtn").on("click", FN.chooseRole);
        },
        chooseRole : () => {

            const channel = ModalChannel.Builder
                                        .elementId("search-role")
                                        .receiver(FN.addRole)
                                        .build();

            let exist = $("input:hidden[name^='roles['][name$='].id']")
                              .map((idx, el) => parseInt($(el).val()))
                              .get();

            channel.send(exist);
        },
        addRole : (event) => {

            $($.templates("#addRole").render(event.data)).appendTo("#tbody-role");
            $("input:hidden[name^='roles['][name$='].id']")
                .each((idx, el) => $(el).attr("name", "roles".concat("[", idx, "].id")));

            $("#roleModal").modal("hide");
        },
        removeRow : (el) => {
            $(el).parent().parent().remove();
        }
    }
}();

$(function(){
    $(document).ready(function(){
        FN.initialize();
    });
});