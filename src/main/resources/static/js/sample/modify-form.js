/**
 * @since       2021.12.13
 * @author      preah
 * @description sample modify
 **********************************************************************************************************************/
const FN = function () {
    return {
        initialize: function () {
        }
    }
}();

$(function(){
    $(document).ready(function(){
        FN.initialize();
    });
});