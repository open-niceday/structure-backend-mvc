/**
 * CKEditorHandler 상세 스크립트
 * @author minam
 * @since 2021.03.04
 */
var CKEditorHandler = CKEditorHandler  || (function(){
    var fn = {
        create: function(textareaId) {

            /*
                CKEditor Install
            */
            CKEDITOR.replace( textareaId ).on('change', function(content, data){
                $('#' + textareaId).text(this.getData());
            });

            CKEDITOR.on('instanceReady', function(ev){
                ev.editor.dataProcessor.htmlFilter.addRules({
                    elements: {
                        ol: function(e) {
                            e.attributes.style = 'list-style: decimal';
                        },
                        ul: function(e) {
                            e.attributes.style = 'list-style: circle';
                        }
                    }
                });
            });
        }
    };

    return { "FN": fn, "DataAction": fn }
})();