/**
 * <p>jquery.ajax 사용 편의를 위한 공통 스크립트</p>
 * @author minam
 * @since 2021.03.02
 */

var currentAjaxRequest; // 전역변수
var JqueryAjax = JqueryAjax || (function() {
    /**
     * <p>현재 페이지 URL에서 파라미터 추출 </p>
     * @author minam
     * @from http://antop.tistory.com/60
     */
    function getParams() {
        // 파라미터가 담길 배열
        var param = []; // modifiedBy minam 

        // 현재 페이지의 url
        var url = decodeURIComponent(window.location.href);
        // url이 encodeURIComponent 로 인코딩 되었을때는 다시 디코딩 해준다.
        url = decodeURIComponent(url);

        // url에서 '?' 문자 이후의 파라미터 문자열까지 자르기
        if (url.indexOf('?') !== -1 && url.indexOf("=") !== -1) {
            var params = url.substring( url.indexOf('?')+1, url.length );

            // 파라미터 구분자("&") 로 분리
            params = params.split("&");

            // params 배열을 다시 "=" 구분자로 분리하여 param 배열에 key = value 로 담는다.
            var size = params.length;
            var key, value;
            for(var i=0 ; i < size ; i++) {
                if (params[i].indexOf("=") !== -1) {
                    key = params[i].split("=")[0];
                    value = params[i].split("=")[1];

                    param.push({"key": key, "value": value}); // modifiedBy minam
                }
            }
        }

        return param;
    }

    var fn = {

        /**
         * <p>현재 페이지의 주소와 파라미터를 가공한다.</p>
         * @author minam
         * @return reqUrl (encodedUrl)
         */
        makeRequestUrl: function() {
            var params = getParams();
            var paramString = "";
            _.map (params, function(param, index){
                paramString += param.key + "=" + param.value +"&";
            });

            var reqUrl = window.location.toString().match(/\/\/[^\/]+\/([^\.]+)/)[1] +"?" + paramString;

            return encodeURIComponent(reqUrl.substring(reqUrl.indexOf("/")+1, reqUrl.length));
        }
    };

    var dataAction = {

        /**
         * <p>AJAX 사용</p>
         * <pre>Jquery AJAX를 편리하게 사용하기 위해 만든 함수</pre>
         *
         * @param sendURL: 요청주소입력 (String)
         * @param data: 요청 파라미터 (Array{})
         * @param otherOptions: jquery ajax 의 다른 옵션을 변경할 때 사용 (Array{})
         * @param resultFunction: 처리 후 수행할 함수 (function())
         * @param resultValue: 처리 후 수행할 함수에 전달하는 값 (서버 처리와 상관관계없음)
         * @param forceAbortPR: 현재 요청이 있을 경우 강제 종료할 것인가에 대한 여부 (boolean)
         *
         * @author minam
         */
        fnAjax : function({sendURL, data, otherOptions, resultFunction, resultValue, forceAbortPR}) {

            var headers = {
                'Accept': 'application/json'
            };

            if(otherOptions && otherOptions.headers) {
                headers = $.extend({}, headers, otherOptions.headers);
            }

            var ajaxDelayLimitCount = 30; //second (Default 30)
            var ajaxProcessCount = 0;		// second

            /**
             * <p>지연메세지 발생됨. toastr.js 라이브러리 이용함</p>
             * @author minam
             */
            function showDelayToast() {
                ajaxProcessCount++;
                if (ajaxProcessCount >= ajaxDelayLimitCount) {
                    JqueryToastr.FN.show("처리지연안내", "처리가 지연되고 있습니다. 창을 닫지 마시고, 잠시만 기다려 주세요. 가능한 빨리 처리하겠습니다.", {
                        positionClass: "toast-top-full-width"
                    });
                    ajaxProcessCount = 0;
                }
            }

            var ajaxTimeCounter = setInterval(showDelayToast, 1000);

            var ajaxOptions = {
                type : "get",
                url : sendURL,
                dataType : "json",
                headers: headers,
                data : data,
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    if (errorThrown === "abort") {
                        console.log("responseText: " + XMLHttpRequest.responseText
                            + ", errorThrown: " + errorThrown);
                        console.log("textStatus: " + textStatus + ", \n errorThrown: "
                            + errorThrown);
                    } else if (XMLHttpRequest.status === "401") {
                        alert("세션이 만료되었습니다.");
                    }
                },
                success : function(data) {
                    resultFunction(data, resultValue);
                    window.location.hash = "";
                },
                beforeSend : function(xmlHttpRequest) {
                    // 통신을 시작할때 처리
                    xmlHttpRequest.setRequestHeader("AJAX", "true"); // ajax 호출을  header에 기록
                },
                complete : function() {
                    // 통신이 완료된 후 처리
                    clearInterval(ajaxTimeCounter);
                    JqueryToastr.FN.hide();
                }
            };

            ajaxOptions = $.extend({}, ajaxOptions, otherOptions);

            // 아직 실행 중인 ajax 가 있을 경우 취소한다. // forceAbortPR = true;
            if (forceAbortPR && currentAjaxRequest
                && currentAjaxRequest.readystate !== 4) {
                try {
                    currentAjaxRequest.abort();
                } catch (err) {
                    console.log("common.js > ajax abort()");
                } finally {
                    currentAjaxRequest = null;
                }
            }

            currentAjaxRequest = $.ajax(ajaxOptions);
        },

        /**
         * <p>
         * 동기화 방식으로 ajax 를 호출함.
         * </p>
         *
         * <pre>
         * @WARNING
         * 		Synchronous XMLHttpRequest on the main thread is deprecated because of its detrimental effects to the end user's experience
         * </pre>
         * @author minam
         * @param sendURL {String}
         * @param data {Array}
         * @returns responseText {JSON Array}
         */
        fnSynchronizeAjax : function(sendURL, data, otherOptions) {
            var ajaxOptions = {
                type : "POST",
                url : sendURL,
                dataType : "json",
                data : data,
                async : false, // Synchronous XMLHttpRequest on the main thread is deprecated because of its detrimental effects to the end user's experience
                beforeSend : function(xmlHttpRequest) {
                    // 통신을 시작할때 처리

                    xmlHttpRequest.setRequestHeader("AJAX", "true"); // ajax 호출을  header에 기록
                },
                complete : function() {
                    // 통신이 완료된 후 처리
                }
            };
            ajaxOptions = $.extend({}, ajaxOptions, otherOptions);

            return $.ajax(ajaxOptions).responseText;
        }
    }

    return {
        "FN": fn,
        "DataAction": dataAction
    }
})();