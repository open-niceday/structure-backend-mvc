/**
 * <p>jquery.toastr 사용을 위한 공통 스크립트</p>
 * @author minam
 * @since 2021.03.02
 */

let JqueryToastr = (function() {
    let $toastr = null; // 마지막 최근 toastr 
    let fn = {
        getToast: function() {
            return $toastr;
        },

        show: function(title, msg, paramOptions) {

            if ($toastr == null) {
                let options = {
                    positionClass: "toast-bottom-right", // toast-top-right, toast-bottom-right, toast-bottom-left, toast-top-left, toast-top-full-width, toast-bottom-full-width
                    shortCutFunction: "info", // success, info, warning, error
                    showDuration: 200,
                    hideDuration: 1500,
                    timeOut: 2000,
                    extendedTimeout: 0,
                    showEasing: "swing", // swing, linear
                    hideEasing: "swing", // swing, linear
                    showMethod: "fadeIn", // show, fadeIn, slideDown
                    hideMethod: "fadeOut", // hide, fadeOut, slideUp
                    onclick: null, // function () {}
                    closeButton: true,
                    debug: false
                };

                toastr.options = $.extend({}, options, paramOptions);

                if (!msg) {
                    msg = "입력된 내용이 없습니다. 메세지 내용을 입력하세요.";
                }
                $toastr = toastr[toastr.options.shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
            }
        },

        hide: function() {
            if ($toastr) {
                toastr.clear($toastr);
            }

        }
    }

    return {
        FN: fn
    }
})();