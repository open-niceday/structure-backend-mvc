/**
 * @since       2022.01.12
 * @author      preah
 * @description sidebar
 **********************************************************************************************************************/
const Sidebar = function () {
    return {
        initialize: () => {

            const paths = window.location.pathname.split("/");
            const current = paths.splice(0, paths.length-1).join("/");

            $('ul#sidebarnav a').each(function(index, element) {
                if(this.href.indexOf(current) > -1) {
                    $(element).addClass('active');
                    $(element).parentsUntil('.sidebar-nav').each(function () {

                        if ($(this).is('li') && $(this).children('a').length !== 0) {
                            $(this).children('a').addClass('active');
                            $(this).addClass($(this).parent('ul#sidebarnav').length === 0 ? "active" : "selected");
                            return;
                        }

                        if (!$(this).is('ul') && $(this).children('a').length === 0) {
                            $(this).addClass('selected');
                            return;
                        }

                        if ($(this).is('ul')) {
                            $(this).addClass('in');
                            return;
                        }
                        return;
                    });
                }

                $(element).on("click", function (e) {
                    if (!$(this).hasClass("active")) {
                        $(this).next("ul").addClass("in");
                        $(this).addClass("active");
                        return;
                    }
                    if ($(this).hasClass("active")) {
                        $(this).removeClass("active");
                        $(this).parents("ul:first").removeClass("active");
                        $(this).next("ul").removeClass("in");
                        return;
                    }
                    return;;
                });
            });

            $("#sidebarnav > li > a.has-arrow").on("click", function (e) {
                e.preventDefault();
            });
        }
    }
}();

$(function(){
    $(document).ready(function(){
        Sidebar.initialize();
    });
});