/**
 * @since       2022.01.07
 * @author      preah
 * @description modal channel
 **********************************************************************************************************************/
class ModalChannel {

    constructor(build) {
        if(build) {
            this.channel = build.channel;
        }
    }

    static get Builder() {
        class Builder {
            elementId(elementId) {
                this.channel = new MessageChannel();
                const iframe = document.getElementById(elementId);
                iframe.contentWindow.postMessage({}, "*", [this.channel.port2]);
                return this;
            }
            receiver(method) {
                this.channel.port1.onmessage = method;
                return this;
            }
            build() {
                return new ModalChannel(this);
            }
        }
        return new Builder();
    }

    send(message) {
        this.channel.port1.postMessage(message);
    }
}

/**
 * @since       2022.01.07
 * @author      preah
 * @description modal listener
 **********************************************************************************************************************/
class ModalListener {

    constructor(build) {
        if(build) {
            window.addEventListener("message", (event)=> {
                let port2;
                [port2] = event.ports;
                this.port2 = port2;
                port2.onmessage = build.method;
            });
        }
    }

    static get Builder() {
        class Builder {

            receiver(method) {
                this.method = method;
                return this;
            }

            build() {
                return new ModalListener(this);
            }
        }
        return new Builder();
    }

    send(message) {
        this.port2.postMessage(message);
    }
}