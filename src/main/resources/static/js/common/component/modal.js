/**
 * @since       2021.12.13
 * @author      preah
 * @description modal
 **********************************************************************************************************************/
$(function(){
    $(document).ready(function(){

        const mutationObserver = new MutationObserver((record, observer)=> {
            record.forEach(mutation => {
                if(mutation.attributeName === 'class'){
                    document.querySelectorAll("[class='modal-frame']").forEach((el) => {
                        el.height = el.contentWindow.document.body.scrollHeight;
                        el.style.overflow = "hidden";
                        $(el).attr("src", $(el).attr("src"));
                    });
                }
            });
        });

        document.querySelectorAll("[role='dialog']").forEach((el) => {
           mutationObserver.observe(el, { attributes: true });
        });
    });
});