create table account
(
	id         bigint       not null auto_increment,
	identifier varchar(100) not null,
	name       varchar(100) not null,
	email      varchar(100) not null,
	password   varchar(100),
	created_by bigint,
	created_at datetime,
	updated_by bigint,
	updated_at datetime,
	primary key (id)
) auto_increment = 1;

create table role
(
	id          bigint      not null auto_increment,
	role_type   varchar(30) not null,
	description varchar(500),
	created_by  bigint      not null,
	created_at  datetime    not null,
	updated_by  bigint,
	updated_at  datetime,
	primary key (id)
) auto_increment = 1;

create table account_role
(
	account_id bigint not null,
	role_id    bigint not null,
	primary key (account_id, role_id),
	foreign key (account_id) references account (id),
	foreign key (role_id) references role (id)
);

create table resource
(
	id          bigint       not null auto_increment,
	name        varchar(300) not null,
	pattern     varchar(100) not null,
	description varchar(500),
	created_by  bigint       not null,
	created_at  datetime     not null,
	updated_by  bigint,
	updated_at  datetime,
	primary key (id)
) auto_increment = 1;

create table role_resource
(
	role_id     bigint not null,
	resource_id bigint not null,
	primary key (role_id, resource_id),
	foreign key (role_id) references role (id),
	foreign key (resource_id) references resource (id)
);

create table menu
(
	id         bigint       not null auto_increment,
	up_id      bigint,
	name       varchar(100) not null,
	sort       int          not null,
	url        varchar(200) not null,
	use_yn     tinyint,
	created_by bigint,
	created_at datetime,
	updated_by bigint,
	updated_at datetime,
	primary key (id),
	foreign key (up_id) references menu (id)
) auto_increment = 1;


create table role_menu
(
	role_id     bigint not null,
	menu_id     bigint not null,
	primary key (role_id, menu_id),
	foreign key (role_id) references role (id),
	foreign key (menu_id) references menu (id)
);