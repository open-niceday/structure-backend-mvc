/* password : 12345678 */
insert into account
(identifier, name, email, password, created_by, created_at, updated_by, updated_at)
values('admin', '관리자', 'admin@niceday.io', '$2a$10$1Kmy.MLRKDuZtIoOsVNNf.lZeWGdGeFHJSpnTCTbyx2hHbZ.5OQmm', 1, now(), 1, now());


insert into `role`
(role_type, description, created_by, created_at, updated_by, updated_at) values
 ('ROLE_ADMIN', '관리자권한', 1, now(), 1, now())
,('ROLE_USER', '사용자', 1, now(), 1, now())
,('ROLE_ANONYMOUS', '익명사용자', 1, now(), 1, now());


insert into resource
(name, pattern, description, created_by, created_at, updated_by, updated_at) values
 ('비지니스 공통 영역', '/common/**', '비지니스 공통 영역', 1, now(), 1, now())
,('독립적 실행 영역', '/alone/**', '독립적 실행 영역', 1, now(), 1, now())
,('비지니스 지원 영역', '/assist/**', '비지니스 지원 영역', 1, now(), 1, now())
,('비지니스 메인 영역', '/basis/**', '비지니스 메인 영역', 1, now(), 1, now())
,('샘플', '/sample/**', '샘플', 1, now(), 1, now());


insert into role_resource
(role_id, resource_id)
select b.id, a.id
from resource a
inner join `role` b on 1=1
and b.role_type not in ('ROLE_ANONYMOUS');

insert into account_role
(account_id, role_id)
select a.id, b.id
from account a
inner join `role` b on 1=1
and b.role_type not in ('ROLE_ANONYMOUS');


INSERT INTO menu (up_id,name,sort,url,use_yn,created_by,created_at,updated_by,updated_at) VALUES
 (NULL,'ROOT',1,'/',1,1,now(),NULL,NULL)
,(1,'시스템관리',1,'',1,1,now(),NULL,NULL)
,(1,'샘플',2,'/sample/page',1,1,now(),NULL,NULL)
,(2,'사용자',1,'/access/account/user/page',1,1,now(),NULL,NULL)
,(2,'권한',2,'/access/role/page',1,1,now(),NULL,NULL)
,(2,'자원',3,'/access/resource/page',1,1,now(),NULL,NULL)
,(2,'메뉴',4,'/access/menu/tree',1,1,now(),NULL,NULL)
;

insert into role_menu
(role_id, menu_id)
select b.id, a.id
from menu a
inner join `role` b on 1=1
and b.role_type not in ('ROLE_ANONYMOUS')
and a.name <> 'ROOT';