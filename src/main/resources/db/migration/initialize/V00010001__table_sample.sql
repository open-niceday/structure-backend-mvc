create table school
(
	id          bigint      not null auto_increment,
	name        nvarchar(100) not null,
	school_type varchar(50) not null,
	created_by  bigint      not null,
	created_at  datetime    not null,
	updated_by  bigint,
	updated_at  datetime,
	primary key (id)
) auto_increment = 1;
