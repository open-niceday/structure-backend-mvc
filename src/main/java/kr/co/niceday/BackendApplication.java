package kr.co.niceday;

import kr.co.niceday.common.engine.config.bean.NameGenerator;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @since       2022.01.11
 * @author      preah
 * @description backend application
 **********************************************************************************************************************/
@SpringBootApplication
@ComponentScan(nameGenerator= NameGenerator.class)
@MapperScan(basePackages="kr.co.niceday", nameGenerator=NameGenerator.class)
public class BackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);
    }

}
