package kr.co.niceday.cms.access.role.form;

import io.swagger.annotations.ApiModelProperty;
import kr.co.niceday.cms.access.role.enumerate.RoleType;
import kr.co.niceday.common.base.form.BaseForm;
import lombok.*;

import java.time.LocalDateTime;

/**
 * @since       2021.12.23
 * @author      preah
 * @description role modal form
 **********************************************************************************************************************/
public class RoleModalForm {

    public static class SearchPage {

        public static class Request {

            @Getter
            @Setter
            @Builder(toBuilder=true)
            @NoArgsConstructor
            @AllArgsConstructor
            public static class Find {

                @ApiModelProperty(value="권한구분")
                private RoleType roleType;
            }
        }

        public static class Response {

            @Data
            public static class FindAll {

                @ApiModelProperty(value="권한일련번호")
                private Long id;

                @ApiModelProperty(value="권한구분")
                private RoleType roleType;

                @ApiModelProperty(value="설명")
                private String description;

                @ApiModelProperty(value="등록일시")
                private LocalDateTime createdAt;

                @ApiModelProperty(value="생성자")
                private BaseForm.Response.Account creator;
            }
        }
    }
}
