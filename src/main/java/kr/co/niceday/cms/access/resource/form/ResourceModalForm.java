package kr.co.niceday.cms.access.resource.form;

import io.swagger.annotations.ApiModelProperty;
import kr.co.niceday.common.base.form.BaseForm;
import lombok.*;

import java.time.LocalDateTime;

/**
 * @since       2021.12.23
 * @author      preah
 * @description resource modal form
 **********************************************************************************************************************/
public class ResourceModalForm {

    public static class SearchPage {

        public static class Request {

            @Getter
            @Setter
            @Builder(toBuilder=true)
            @NoArgsConstructor
            @AllArgsConstructor
            public static class Find {

                @ApiModelProperty(value="이름")
                private String name;
            }
        }

        public static class Response {

            @Data
            public static class FindAll {

                @ApiModelProperty(value="자원일련번호")
                private Long id;

                @ApiModelProperty(value="이름")
                private String name;

                @ApiModelProperty(value="패턴")
                private String pattern;

                @ApiModelProperty(value="설명")
                private String description;

                @ApiModelProperty(value="등록일시")
                private LocalDateTime createdAt;

                @ApiModelProperty(value="생성자")
                private BaseForm.Response.Account creator;
            }
        }
    }
}
