package kr.co.niceday.cms.access.menu.mapper;

import kr.co.niceday.cms.access.menu.entity.Menu;
import kr.co.niceday.cms.access.menu.form.MenuModalForm.SearchTree;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @since       2021.12.13
 * @author      preah
 * @description menu modal mapper
 **********************************************************************************************************************/
@Mapper
public interface MenuModalMapper {

    MenuModalMapper mapper = Mappers.getMapper(MenuModalMapper.class);

    SearchTree.Response.FindOne toFindOne(Menu entity);
}
