package kr.co.niceday.cms.access.account.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @since       2021.12.15
 * @author      preah
 * @description account form
 **********************************************************************************************************************/
public class AccountForm {

    public static class Response {

        @Data
        public static class Sidebar implements Serializable {

            @ApiModelProperty(value="메뉴일련번호")
            private Long id;

            @ApiModelProperty(value="이름")
            private String name;

            @ApiModelProperty(value="순서")
            private Integer sort;

            @ApiModelProperty(value="주소")
            private String url;

            @ApiModelProperty(value="사용여부")
            private Boolean useYn;

            @ApiModelProperty(value="메뉴-자식")
            private List<Child> children;

            @Data
            public static class Child implements Serializable {

                @ApiModelProperty(value="메뉴일련번호")
                private Long id;

                @ApiModelProperty(value="이름")
                private String name;

                @ApiModelProperty(value="순서")
                private Integer sort;

                @ApiModelProperty(value="주소")
                private String url;

                @ApiModelProperty(value="사용여부")
                private Boolean useYn;

                @ApiModelProperty(value="메뉴-자식")
                private List<Child> children;
            }

            @ApiModelProperty(value="네이게이터")
            private List<Navigate> navigates;

            @Data
            @Builder
            @AllArgsConstructor
            public static class Navigate implements Serializable {

                @ApiModelProperty(value="URL")
                private String url;

                @ApiModelProperty(value="이름")
                private String name;

                @ApiModelProperty(value="경로")
                private String path;
            }
        }
    }
}
