package kr.co.niceday.cms.access.menu.exception;

import kr.co.niceday.common.engine.exception.common.ExceptionCode;
import org.springframework.http.HttpStatus;

/**
 * @since       2021.12.20
 * @author      preah
 * @description menu exception
 **********************************************************************************************************************/
@SuppressWarnings("serial")
public class MenuException extends RuntimeException {

	public MenuException(){
		super(ExceptionCode.E00100006.name());
	}

	public MenuException(ExceptionCode exceptionCode, HttpStatus httpStatus){
		super(exceptionCode.name());
		code   = exceptionCode;
		status = httpStatus;
	}

	public MenuException(ExceptionCode exceptionCode, Exception exception){
		super(exceptionCode.name(), exception);
		code = exceptionCode;
	}
	public MenuException(ExceptionCode exceptionCode){
		super(exceptionCode.name());
		code   = exceptionCode;
	}

	public static ExceptionCode code   = ExceptionCode.E00100006;
	public static HttpStatus    status = HttpStatus.NOT_ACCEPTABLE;
}
