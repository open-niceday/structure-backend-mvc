package kr.co.niceday.cms.access.account.mapper;

import kr.co.niceday.cms.access.menu.entity.Menu;
import kr.co.niceday.cms.access.account.form.AccountForm.Response;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

/**
 * @since       2021.12.13
 * @author      preah
 * @description menu mapper
 **********************************************************************************************************************/
@Mapper(builder=@Builder(disableBuilder=true))
public interface AccountMapper {

    AccountMapper mapper = Mappers.getMapper(AccountMapper.class);

    Response.Sidebar toSidebar(Menu root, @Context MenuContext context);
}