package kr.co.niceday.cms.access.menu.mapper;

import kr.co.niceday.cms.access.menu.entity.Menu;
import kr.co.niceday.cms.access.menu.form.MenuForm.Request;
import kr.co.niceday.cms.access.menu.form.MenuForm.Response;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

/**
 * @since       2021.12.13
 * @author      preah
 * @description menu mapper
 **********************************************************************************************************************/
@Mapper
public interface MenuMapper {

    MenuMapper mapper = Mappers.getMapper(MenuMapper.class);

    Menu toMenu(Request.Add form);
    Menu toMenu(Request.Modify form);

    Response.FindOne toFindOne(Menu entity);

    @Mapping(target="id",       ignore=true)
    @Mapping(target="parent",   ignore=true)
    @Mapping(target="children", ignore=true)
    @Mapping(target="roles",    ignore=true)
    Menu modify(Menu source, @MappingTarget Menu target);
}
