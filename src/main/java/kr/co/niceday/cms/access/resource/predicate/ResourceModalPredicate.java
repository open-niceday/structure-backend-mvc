package kr.co.niceday.cms.access.resource.predicate;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import kr.co.niceday.cms.access.resource.entity.QResource;
import kr.co.niceday.cms.access.resource.form.ResourceModalForm.SearchPage;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * @since       2021.12.15
 * @author      preah
 * @description resource modal predicate
 **********************************************************************************************************************/
public class ResourceModalPredicate {

    public static Predicate search(SearchPage.Request.Find find) {
        QResource resource = QResource.resource;
        BooleanBuilder builder = new BooleanBuilder();

        if(Objects.nonNull(find)) {
            if(StringUtils.isNotBlank(find.getName())) {
                builder.and(resource.name.contains(find.getName()));
            }
        }
        return builder;
    }
}
