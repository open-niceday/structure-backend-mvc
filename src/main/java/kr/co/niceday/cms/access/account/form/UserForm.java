package kr.co.niceday.cms.access.account.form;

import io.swagger.annotations.ApiModelProperty;
import kr.co.niceday.cms.access.role.enumerate.RoleType;
import kr.co.niceday.common.base.form.BaseForm;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @since       2021.12.15
 * @author      preah
 * @description user form
 **********************************************************************************************************************/
public class UserForm {

    public static class Request {

        @Getter
        @Setter
        @Builder(toBuilder=true)
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Find {

            @ApiModelProperty(value="아이디")
            private String identifier;

            @ApiModelProperty(value="이름")
            private String name;
        }

        @Getter
        @Setter
        @Builder(toBuilder=true)
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Add {

            @ApiModelProperty(value="아이디", required=true)
            @NotBlank
            @Length(min=3, max=100)
            private String identifier;

            @ApiModelProperty(value="이름", required=true)
            @NotBlank
            @Length(max=100)
            private String name;

            @ApiModelProperty(value="이메일", required=true)
            @NotBlank
            @Email
            @Length(max=100)
            private String email;

            @ApiModelProperty(value="비밀번호", required=true)
            @NotBlank
            @Length(min=5, max=100)
            private String password;

            @ApiModelProperty(value="권한목록", required=true)
            @Valid
            @NotEmpty
            private List<Role> roles;

            @Getter
            @Setter
            @Builder(toBuilder=true)
            @NoArgsConstructor
            @AllArgsConstructor
            public static class Role {

                @ApiModelProperty(value="권한일련번호", required=true)
                @NotNull
                private Long id;
            }
        }

        @Getter
        @Setter
        @Builder(toBuilder=true)
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Modify{

            @ApiModelProperty(value="사용자일련번호", required=true)
            @NotNull
            private Long id;

            @ApiModelProperty(value="아이디", required=true)
            @NotBlank
            @Length(min=3, max=100)
            private String identifier;

            @ApiModelProperty(value="이름", required=true)
            @NotBlank
            @Length(max=100)
            private String name;

            @ApiModelProperty(value="이메일", required=true)
            @NotBlank
            @Email
            @Length(max=100)
            private String email;

            @ApiModelProperty(value="비밀번호", required=true)
            @NotBlank
            @Length(min=5, max=100)
            private String password;

            @ApiModelProperty(value="권한목록", required=true)
            @Valid
            @NotEmpty
            private List<Role> roles;

            @Getter
            @Setter
            @Builder(toBuilder=true)
            @NoArgsConstructor
            @AllArgsConstructor
            public static class Role {

                @ApiModelProperty(value="권한일련번호", required=true)
                @NotNull
                private Long id;
            }
        }
    }

    public static class Response {

        @Data
        public static class FindOne {

            @ApiModelProperty(value="사용자일련번호")
            private Long id;

            @ApiModelProperty(value="아이디")
            private String identifier;

            @ApiModelProperty(value="이름")
            private String name;

            @ApiModelProperty(value="이메일")
            private String email;

            @ApiModelProperty(value="비밀번호")
            private String password;

            @ApiModelProperty(value="권한목록")
            private List<Role> roles;

            @Data
            public static class Role {

                @ApiModelProperty(value="권한일련번호")
                private Long id;

                @ApiModelProperty(value="권한구분")
                private RoleType roleType;

                @ApiModelProperty(value="설명")
                private String Description;
            }

            @ApiModelProperty(value="등록일시")
            private LocalDateTime createdAt;

            @ApiModelProperty(value="생성자")
            private BaseForm.Response.Account creator;
        }

        @Data
        public static class FindAll {

            @ApiModelProperty(value="사용자일련번호")
            private Long id;

            @ApiModelProperty(value="아이디")
            private String identifier;

            @ApiModelProperty(value="이름")
            private String name;

            @ApiModelProperty(value="이메일")
            private String email;

            @ApiModelProperty(value="비밀번호")
            private String password;

            @ApiModelProperty(value="권한목록")
            private List<Role> roles;

            @Data
            public static class Role {

                @ApiModelProperty(value="권한일련번호")
                private Long id;

                @ApiModelProperty(value="권한구분")
                private RoleType roleType;
            }

            @ApiModelProperty(value="등록일시")
            private LocalDateTime createdAt;

            @ApiModelProperty(value="생성자")
            private BaseForm.Response.Account creator;
        }
    }
}
