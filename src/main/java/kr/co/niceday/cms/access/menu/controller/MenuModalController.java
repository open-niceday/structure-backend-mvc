package kr.co.niceday.cms.access.menu.controller;

import kr.co.niceday.cms.access.menu.service.MenuService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import static kr.co.niceday.cms.access.menu.mapper.MenuModalMapper.mapper;

/**
 * @since       2021.12.03
 * @author      preah
 * @description menu modal controller
 **********************************************************************************************************************/
@Controller
@RequiredArgsConstructor
@RequestMapping("${property.context.path}")
public class MenuModalController {

    private final MenuService menuService;

    @GetMapping("/access/menu/modal/search-tree")
    public String searchTree(Model model) {
        model.addAttribute("findOne", mapper.toFindOne(menuService.getRoot()));
        return "access/menu/modal/search-tree";
    }
}
