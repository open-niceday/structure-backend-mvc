package kr.co.niceday.cms.access.account.adapter;

import kr.co.niceday.cms.access.account.form.AccountForm.Response;
import kr.co.niceday.cms.access.account.mapper.MenuContext;
import kr.co.niceday.cms.access.menu.service.MenuService;
import kr.co.niceday.cms.access.role.enumerate.RoleType;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.Set;

import static kr.co.niceday.cms.access.account.mapper.AccountMapper.mapper;
/**
 * @since       2021.12.03
 * @author      preah
 * @description account adapter
 **********************************************************************************************************************/
@Component
@RequiredArgsConstructor
public class AccountAdapter {

    private final MenuService menuService;

    @Cacheable(cacheNames="access-account-get-sidebar", key="#roleTypes")
    public Response.Sidebar getSidebar(Set<RoleType> roleTypes) {
        return mapper.toSidebar(menuService.getRoot(), new MenuContext(menuService.getUsed(roleTypes)));
    }
}
