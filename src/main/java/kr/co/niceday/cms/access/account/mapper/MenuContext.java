package kr.co.niceday.cms.access.account.mapper;

import kr.co.niceday.cms.access.account.form.AccountForm.Response;
import kr.co.niceday.cms.access.menu.entity.Menu;
import kr.co.niceday.common.engine.constant.Constant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.BeforeMapping;
import org.mapstruct.MappingTarget;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @since       2023.01.10
 * @author      preah
 * @description menu context
 **********************************************************************************************************************/
public class MenuContext {

    private List<Menu> targets;
    private List<Response.Sidebar.Navigate> navigates;

    @Getter
    @Setter
    @Builder
    @AllArgsConstructor
    public static class Node {

        private Menu   target;
        private String path;
    }

    public MenuContext(List<Menu> menus) {
        this.targets   = menus;
        this.navigates = Lists.newArrayList();
    }

    @BeforeMapping
    public void init(Menu target) {
        if(Objects.nonNull(target)) {

            if(target.getChildren().isEmpty()) {

                this.navigates.add(
                    Response.Sidebar.Navigate.builder()
                            .url (StringUtils.substringBeforeLast(target.getUrl(), Constant.String.SLASH))
                            .name(target.getName())
                            .path(this.toPath(Node.builder().target(target).path(target.getName()).build()).getPath())
                            .build()
                );
            }

            target.setChildren(target.getChildren().stream()
                    .filter (m -> targets.stream().anyMatch(t -> t.equals(m)))
                    .collect(Collectors.toList()));
        }
    }

    @AfterMapping
    public void set(@MappingTarget Response.Sidebar target) {
        target.setNavigates(this.navigates);
    }

    private Node toPath(Node node) {

        if(ObjectUtils.allNotNull(node.getTarget().getParent(), node.getTarget().getParent().getParent())) {

            return this.toPath(Node.builder()
                                   .target(node.getTarget().getParent())
                                   .path  (StringUtils.join(node.getTarget().getParent().getName(), Constant.String.SPACE, Constant.String.RIGHT_ARROW, Constant.String.SPACE, node.getPath()))
                                   .build());
        }
        return node;
    }
}