package kr.co.niceday.cms.access.menu.form;

import io.swagger.annotations.ApiModelProperty;
import kr.co.niceday.common.base.form.BaseForm;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @since       2021.12.23
 * @author      preah
 * @description menu modal form
 **********************************************************************************************************************/
public class MenuModalForm {

    public static class SearchTree {

        public static class Response {

            @Data
            public static class FindOne {

                @ApiModelProperty(value="메뉴일련번호")
                private Long id;

                @ApiModelProperty(value="이름")
                private String name;

                @ApiModelProperty(value="순서")
                private Integer sort;

                @ApiModelProperty(value="주소")
                private String url;

                @ApiModelProperty(value="사용여부")
                private Boolean useYn;

                @ApiModelProperty(value="메뉴-자식")
                private List<Child> children;

                @Data
                public static class Child {

                    @ApiModelProperty(value="메뉴일련번호")
                    private Long id;

                    @ApiModelProperty(value="이름")
                    private String name;

                    @ApiModelProperty(value="순서")
                    private Integer sort;

                    @ApiModelProperty(value="주소")
                    private String url;

                    @ApiModelProperty(value="사용여부")
                    private Boolean useYn;

                    @ApiModelProperty(value="메뉴-부모")
                    private Parent parent;

                    @ApiModelProperty(value="메뉴-자식")
                    private List<Child> children;

                    @ApiModelProperty(value="등록일시")
                    private LocalDateTime createdAt;

                    @ApiModelProperty(value="생성자")
                    private BaseForm.Response.Account creator;
                }

                @ApiModelProperty(value="메뉴-부모")
                private Parent parent;

                @Data
                public static class Parent {

                    @ApiModelProperty(value="메뉴일련번호")
                    private Long id;

                    @ApiModelProperty(value="이름")
                    private String name;

                    @ApiModelProperty(value="순서")
                    private Integer sort;

                    @ApiModelProperty(value="주소")
                    private String url;

                    @ApiModelProperty(value="사용여부")
                    private Boolean useYn;

                    @ApiModelProperty(value="등록일시")
                    private LocalDateTime createdAt;

                    @ApiModelProperty(value="생성자")
                    private BaseForm.Response.Account creator;
                }

                @ApiModelProperty(value="등록일시")
                private LocalDateTime createdAt;

                @ApiModelProperty(value="생성자")
                private BaseForm.Response.Account creator;
            }
        }
    }
}
