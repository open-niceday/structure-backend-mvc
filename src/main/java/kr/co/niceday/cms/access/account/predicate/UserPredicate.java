package kr.co.niceday.cms.access.account.predicate;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import kr.co.niceday.cms.access.account.entity.QAccount;
import kr.co.niceday.cms.access.account.form.UserForm.Request;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * @since       2021.12.15
 * @author      preah
 * @description user predicate
 **********************************************************************************************************************/
public class UserPredicate {

    public static Predicate search(Request.Find find) {
        QAccount account = QAccount.account;
        BooleanBuilder builder = new BooleanBuilder();

        if(Objects.nonNull(find)) {
            if(StringUtils.isNotBlank(find.getName())){
                builder.and(account.name.contains(find.getName()));
            }
            if(Objects.nonNull(find.getIdentifier())) {
                builder.and(account.identifier.contains(find.getIdentifier()));
            }
        }
        return builder;
    }
}
