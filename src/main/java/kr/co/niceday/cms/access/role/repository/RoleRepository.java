package kr.co.niceday.cms.access.role.repository;

import kr.co.niceday.cms.access.role.entity.Role;
import kr.co.niceday.cms.access.role.enumerate.RoleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @since       2021.09.23
 * @author      preah
 * @description role repository
 **********************************************************************************************************************/
@Repository
public interface RoleRepository extends JpaRepository<Role, Long>, QuerydslPredicateExecutor<Role> {

    Optional<List<Role>> findByRoleType  (RoleType roleType);
    boolean              existsByRoleType(RoleType roleType);
}
