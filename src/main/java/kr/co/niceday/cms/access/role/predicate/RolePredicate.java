package kr.co.niceday.cms.access.role.predicate;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import kr.co.niceday.cms.access.role.entity.QRole;
import kr.co.niceday.cms.access.role.form.RoleForm.Request;

import java.util.Objects;
import java.util.Optional;

/**
 * @since       2021.12.15
 * @author      preah
 * @description role predicate
 **********************************************************************************************************************/
public class RolePredicate {

    public static Predicate search(Request.Find find) {
        QRole role = QRole.role;
        BooleanBuilder builder = new BooleanBuilder();

        if(Objects.nonNull(find)) {
            Optional.ofNullable(find.getRoleType()).ifPresent(roleType -> builder.and(role.roleType.eq(roleType)));
        }
        return builder;
    }
}
