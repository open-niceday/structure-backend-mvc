package kr.co.niceday.cms.access.role.mapper;

import kr.co.niceday.cms.access.role.entity.Role;
import kr.co.niceday.cms.access.role.form.RoleModalForm.SearchPage;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @since       2021.12.13
 * @author      preah
 * @description role modal mapper
 **********************************************************************************************************************/
@Mapper
public interface RoleModalMapper {

    RoleModalMapper mapper = Mappers.getMapper(RoleModalMapper.class);

    SearchPage.Response.FindAll toFindAll(Role entity);
}
