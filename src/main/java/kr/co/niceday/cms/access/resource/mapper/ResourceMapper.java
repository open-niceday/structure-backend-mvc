package kr.co.niceday.cms.access.resource.mapper;

import kr.co.niceday.cms.access.resource.entity.Resource;
import kr.co.niceday.cms.access.resource.form.ResourceForm.Request;
import kr.co.niceday.cms.access.resource.form.ResourceForm.Response;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

/**
 * @since       2021.12.13
 * @author      preah
 * @description resource mapper
 **********************************************************************************************************************/
@Mapper
public interface ResourceMapper {

    ResourceMapper mapper = Mappers.getMapper(ResourceMapper.class);

    Resource toResource(Request.Add form);
    Resource toResource(Request.Modify form);

    Response.FindAll toFindAll(Resource entity);
    Response.FindOne toFindOne(Resource entity);

    @Mapping(target="id",          ignore=true)
    Resource modify(Resource source, @MappingTarget Resource target);
}
