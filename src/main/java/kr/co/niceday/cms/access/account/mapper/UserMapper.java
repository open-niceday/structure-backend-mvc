package kr.co.niceday.cms.access.account.mapper;

import kr.co.niceday.cms.access.account.entity.Account;
import kr.co.niceday.cms.access.account.form.UserForm.Request;
import kr.co.niceday.cms.access.account.form.UserForm.Response;
import kr.co.niceday.cms.access.role.entity.Role;
import kr.co.niceday.common.engine.constant.Constant;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @since       2021.12.15
 * @author      preah
 * @description user mapper
 **********************************************************************************************************************/
@Mapper(imports={Constant.class})
public interface UserMapper {

    UserMapper mapper = Mappers.getMapper(UserMapper.class);

    Account toAccount(Request.Add    form);
    Account toAccount(Request.Modify form);

    Response.FindAll      toFindAll(Account entity);
    @Mapping(target="password", constant=Constant.String.EMPTY)
    Response.FindOne      toFindOne(Account entity);
    List<Response.FindOne.Role> toFindOne(List<Role> entities);

    @Mapping(target="id",       ignore=true)
    Account modify(Account source, @MappingTarget Account target);
}
