package kr.co.niceday.cms.access.resource.controller;

import kr.co.niceday.cms.access.resource.form.ResourceForm.Request;
import kr.co.niceday.cms.access.resource.form.ResourceForm.Response;
import kr.co.niceday.cms.access.resource.predicate.ResourcePredicate;
import kr.co.niceday.cms.access.resource.service.ResourceService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

import static kr.co.niceday.cms.access.resource.mapper.ResourceMapper.mapper;

/**
 * @since       2021.12.03
 * @author      preah
 * @description resource controller
 **********************************************************************************************************************/
@Controller
@RequiredArgsConstructor
@RequestMapping("${property.context.path}")
public class ResourceController {

    private final ResourceService resourceService;

    @GetMapping("/access/resource/page")
    public String page(Model model, @ModelAttribute Request.Find find, @PageableDefault @SortDefault(sort="id", direction=Sort.Direction.DESC) Pageable pageable) {
        Page<Response.FindAll> page = resourceService.getPage(ResourcePredicate.search(find), pageable).map(mapper::toFindAll);
        model.addAttribute("find", find);
        model.addAttribute("page", page);
        return "access/resource/page";
    }

    @GetMapping("/access/resource/detail")
    public String detail(Model model, @RequestParam Long id) {
        model.addAttribute("findOne", mapper.toFindOne(resourceService.get(id)));
        return "access/resource/detail";
    }

    @GetMapping("/access/resource/add-form")
    public String add(Model model) {
        model.addAttribute(model.asMap().getOrDefault("add", Request.Add.builder().build()));
        return "access/resource/add-form";
    }

    @PostMapping("/access/resource/add")
    public String add(@Valid @ModelAttribute Request.Add add) {
        resourceService.add(mapper.toResource(add));
        return "redirect:/access/resource/page";
    }

    @GetMapping("/access/resource/modify-form")
    public String modify(Model model, @RequestParam Long id) {
        model.addAttribute("modify", model.asMap().getOrDefault("modify", mapper.toFindOne(resourceService.get(id))));
        return "access/resource/modify-form";
    }

    @PostMapping("/access/resource/modify")
    public String modify(RedirectAttributes redirectAttributes, @Valid @ModelAttribute Request.Modify modify) {
        redirectAttributes.addAttribute("id", resourceService.modify(mapper.toResource(modify)).getId());
        return "redirect:/access/resource/detail";
    }

    @PostMapping("/access/resource/delete")
    public String delete(@RequestParam Long id) {
        resourceService.remove(id);
        return "redirect:/access/resource/page";
    }
}
