package kr.co.niceday.cms.access.resource.exception;

import kr.co.niceday.common.engine.exception.common.ExceptionCode;
import org.springframework.http.HttpStatus;

/**
 * @since       2021.12.20
 * @author      preah
 * @description resource exception
 **********************************************************************************************************************/
@SuppressWarnings("serial")
public class ResourceException extends RuntimeException {

	public ResourceException(){
		super(ExceptionCode.E00100006.name());
	}

	public ResourceException(ExceptionCode exceptionCode, HttpStatus httpStatus){
		super(exceptionCode.name());
		code   = exceptionCode;
		status = httpStatus;
	}

	public ResourceException(ExceptionCode exceptionCode, Exception exception){
		super(exceptionCode.name(), exception);
		code = exceptionCode;
	}
	public ResourceException(ExceptionCode exceptionCode){
		super(exceptionCode.name());
		code   = exceptionCode;
	}

	public static ExceptionCode code   = ExceptionCode.E00100006;
	public static HttpStatus    status = HttpStatus.NOT_ACCEPTABLE;
}
