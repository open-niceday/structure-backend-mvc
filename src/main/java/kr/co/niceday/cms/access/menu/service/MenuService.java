package kr.co.niceday.cms.access.menu.service;

import kr.co.niceday.cms.access.menu.entity.Menu;
import kr.co.niceday.cms.access.menu.exception.MenuException;
import kr.co.niceday.cms.access.menu.repository.MenuRepository;
import kr.co.niceday.cms.access.role.enumerate.RoleType;
import kr.co.niceday.common.engine.exception.NoFoundException;
import kr.co.niceday.common.engine.exception.common.ExceptionCode;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

import static kr.co.niceday.cms.access.menu.mapper.MenuMapper.mapper;

/**
 * @since       2021.12.16
 * @author      preah
 * @description menu service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class MenuService {

    private final MenuRepository menuRepository;

    @Transactional(readOnly=true)
    public Menu get(Long id) {
        return menuRepository.findById(id).orElseThrow(NoFoundException::new);
    }

    @Transactional(readOnly=true)
    public Menu getRoot() {
        return menuRepository.findByParent(null).orElseThrow(NoFoundException::new);
    }

    public Menu add(Menu menu) {
        return menuRepository.save(menu);
    }

    public Menu modify(Menu menu) {
        return mapper.modify(menu, get(menu.getId()));
    }

    public void remove(Long id) {
        if(CollectionUtils.isNotEmpty(this.get(id).getChildren())) {
            throw new MenuException(ExceptionCode.E00400001);
        }
        menuRepository.deleteById(id);
    }

    @Transactional(readOnly=true)
    public List<Menu> getUsed(Set<RoleType> roleTypes) {
        return menuRepository.findByUseYnAndRolesRoleTypeIn(Boolean.TRUE, roleTypes);
    }
}
