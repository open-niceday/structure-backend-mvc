package kr.co.niceday.cms.access.role.controller;

import kr.co.niceday.cms.access.role.form.RoleModalForm.SearchPage;
import kr.co.niceday.cms.access.role.predicate.RoleModalPredicate;
import kr.co.niceday.cms.access.role.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import static kr.co.niceday.cms.access.role.mapper.RoleModalMapper.mapper;

/**
 * @since       2022.01.20
 * @author      preah
 * @description role modal controller
 **********************************************************************************************************************/
@Controller
@RequiredArgsConstructor
@RequestMapping("${property.context.path}")
public class RoleModalController {

    private final RoleService roleService;

    @GetMapping("/access/role/modal/search-page")
    public String searchPage(Model model, @ModelAttribute SearchPage.Request.Find find, @PageableDefault @SortDefault(sort="id", direction=Sort.Direction.DESC) Pageable pageable) {
        Page<SearchPage.Response.FindAll> page = roleService.getPage(RoleModalPredicate.search(find), pageable).map(mapper::toFindAll);
        model.addAttribute("find", find);
        model.addAttribute("page", page);
        return "access/role/modal/search-page";
    }
}
