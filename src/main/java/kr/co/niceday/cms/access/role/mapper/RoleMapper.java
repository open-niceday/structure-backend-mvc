package kr.co.niceday.cms.access.role.mapper;

import kr.co.niceday.cms.access.menu.entity.Menu;
import kr.co.niceday.cms.access.resource.entity.Resource;
import kr.co.niceday.cms.access.role.entity.Role;
import kr.co.niceday.cms.access.role.form.RoleForm.Request;
import kr.co.niceday.cms.access.role.form.RoleForm.Response;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @since       2021.12.13
 * @author      preah
 * @description role mapper
 **********************************************************************************************************************/
@Mapper
public interface RoleMapper {

    RoleMapper mapper = Mappers.getMapper(RoleMapper.class);

    Role toRole(Request.Add    form);
    Role toRole(Request.Modify form);

    Response.FindOne                toFindOne(Role entity);
    List<Response.FindOne.Resource> toFindOne(List<Resource> entities);
    Response.FindOne.Menu           toFindOne(Menu entity);
    Response.FindAll                toFindAll(Role entity);

    @Mapping(target="id",       ignore=true)
    Role modify(Role source, @MappingTarget Role target);
}
