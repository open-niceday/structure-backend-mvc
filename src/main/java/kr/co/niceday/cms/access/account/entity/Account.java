package kr.co.niceday.cms.access.account.entity;

import kr.co.niceday.common.base.entity.Base;
import kr.co.niceday.common.engine.annotation.entity.Description;
import kr.co.niceday.cms.access.role.entity.Role;
import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * @since       2021.12.20
 * @author      preah
 * @description account
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity
@Table(name="account")
@Description("사용자")
public class Account extends Base {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Description("사용자일련번호")
    @Column(nullable=false)
    private Long id;

    @Description("아이디")
    @Column(nullable=false, length=100)
    private String identifier;

    @Description("이름")
    @Column(nullable=false, length=100)
    private String name;

    @Description("이메일")
    @Column(nullable=false, length=100)
    private String email;

    @Description("비밀번호")
    @Column(length=100)
    private String password;

    @Description("권한목록")
    @OneToMany(fetch=FetchType.LAZY)
    @JoinTable(name="account_role", joinColumns=@JoinColumn(name="account_id"), inverseJoinColumns=@JoinColumn(name="role_id"))
    private List<Role> roles;
}
