package kr.co.niceday.cms.access.resource.service;

import com.querydsl.core.types.Predicate;
import kr.co.niceday.cms.access.resource.entity.Resource;
import kr.co.niceday.cms.access.resource.exception.ResourceException;
import kr.co.niceday.cms.access.resource.repository.ResourceRepository;
import kr.co.niceday.common.engine.exception.NoFoundException;
import kr.co.niceday.common.engine.exception.common.ExceptionCode;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static kr.co.niceday.cms.access.resource.mapper.ResourceMapper.mapper;

/**
 * @since       2021.12.16
 * @author      preah
 * @description resource service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class ResourceService {

    private final ResourceRepository resourceRepository;

    @Transactional(readOnly=true)
    public Page<Resource> getPage(Predicate predicate, Pageable pageable) {
        return resourceRepository.findAll(predicate, pageable);
    }

    @Transactional(readOnly=true)
    public Resource get(Long id) {
        return resourceRepository.findById(id).orElseThrow(NoFoundException::new);
    }

    @Transactional(readOnly=true)
    public List<Resource> gets() {
        return resourceRepository.findAll();
    }

    public Resource add(Resource resource) {
        return resourceRepository.save(resource);
    }

    public Resource modify(Resource resource) {
        return mapper.modify(resource, get(resource.getId()));
    }

    public void remove(Long id) {
        if(CollectionUtils.isNotEmpty(this.get(id).getRoles())) {
            throw new ResourceException(ExceptionCode.E00300001);
        }
        resourceRepository.deleteById(id);
    }
}
