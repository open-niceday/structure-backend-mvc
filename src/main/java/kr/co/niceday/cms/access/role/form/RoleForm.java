package kr.co.niceday.cms.access.role.form;

import io.swagger.annotations.ApiModelProperty;
import kr.co.niceday.cms.access.role.enumerate.RoleType;
import kr.co.niceday.common.base.form.BaseForm;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @since       2021.12.23
 * @author      preah
 * @description role form
 **********************************************************************************************************************/
public class RoleForm {

    public static class Request {

        @Getter
        @Setter
        @Builder(toBuilder=true)
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Find {

            @ApiModelProperty(value="권한구분")
            private RoleType roleType;
        }

        @Getter
        @Setter
        @Builder(toBuilder=true)
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Add {

            @ApiModelProperty(value="권한구분", required=true)
            @NotNull
            private RoleType roleType;

            @ApiModelProperty(value="설명")
            @Length(max=500)
            private String description;

            @ApiModelProperty(value="자원목록", required=true)
            @Valid
            @NotEmpty
            private List<Resource> resources;

            @Getter
            @Setter
            @Builder(toBuilder=true)
            @NoArgsConstructor
            @AllArgsConstructor
            public static class Resource {

                @ApiModelProperty(value="자원일련번호", required=true)
                @NotNull
                private Long id;
            }

            @ApiModelProperty(value="메뉴목록", required=true)
            @Valid
            @NotEmpty
            private List<Menu> menus;

            @Getter
            @Setter
            @Builder(toBuilder=true)
            @NoArgsConstructor
            @AllArgsConstructor
            public static class Menu {

                @ApiModelProperty(value="메뉴일련번호", required=true)
                @NotNull
                private Long id;
            }
        }

        @Getter
        @Setter
        @Builder(toBuilder=true)
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Modify {

            @ApiModelProperty(value="권한일련번호", required=true)
            @NotNull
            private Long id;

            @ApiModelProperty(value="권한구분", required=true)
            @NotNull
            private RoleType roleType;

            @ApiModelProperty(value="설명")
            @Length(max=500)
            private String description;

            @ApiModelProperty(value="자원목록", required=true)
            @Valid
            @NotEmpty
            private List<Resource> resources;

            @Getter
            @Setter
            @Builder(toBuilder=true)
            @NoArgsConstructor
            @AllArgsConstructor
            public static class Resource {

                @ApiModelProperty(value="자원일련번호", required=true)
                @NotNull
                private Long id;
            }

            @ApiModelProperty(value="메뉴목록", required=true)
            @Valid
            @NotEmpty
            private List<Menu> menus;

            @Getter
            @Setter
            @Builder(toBuilder=true)
            @NoArgsConstructor
            @AllArgsConstructor
            public static class Menu {

                @ApiModelProperty(value="메뉴일련번호", required=true)
                @NotNull
                private Long id;
            }
        }
    }

    public static class Response {

        @Data
        public static class FindOne {

            @ApiModelProperty(value="권한일련번호")
            private Long id;

            @ApiModelProperty(value="권한구분")
            private RoleType roleType;

            @ApiModelProperty(value="설명")
            private String description;
            
            @ApiModelProperty(value="자원목록")
            private List<Resource> resources;

            @Data
            public static class Resource {

                @ApiModelProperty(value="자원일련번호")
                private Long id;

                @ApiModelProperty(value="이름")
                private String name;

                @ApiModelProperty(value="패턴")
                private String pattern;
            }

            @ApiModelProperty(value="메뉴목록")
            private List<Menu> menus;

            @Data
            public static class Menu {

                @ApiModelProperty(value="메뉴일련번호")
                private Long id;

                @ApiModelProperty(value="이름")
                private String name;

                @ApiModelProperty(value="순서")
                private Integer sort;

                @ApiModelProperty(value="주소")
                private String url;

                @ApiModelProperty(value="사용여부")
                private Boolean useYn;

                @ApiModelProperty(value="메뉴-자식")
                private List<Child> children;

                @Data
                public static class Child {

                    @ApiModelProperty(value="메뉴일련번호")
                    private Long id;

                    @ApiModelProperty(value="이름")
                    private String name;

                    @ApiModelProperty(value="순서")
                    private Integer sort;

                    @ApiModelProperty(value="주소")
                    private String url;

                    @ApiModelProperty(value="사용여부")
                    private Boolean useYn;

                    @ApiModelProperty(value="메뉴-자식")
                    private List<Child> children;

                    @ApiModelProperty(value="등록일시")
                    private LocalDateTime createdAt;

                    @ApiModelProperty(value="생성자")
                    private BaseForm.Response.Account creator;
                }

                @ApiModelProperty(value="등록일시")
                private LocalDateTime createdAt;

                @ApiModelProperty(value="생성자")
                private BaseForm.Response.Account creator;
            }

            @ApiModelProperty(value="등록일시")
            private LocalDateTime createdAt;

            @ApiModelProperty(value="생성자")
            private BaseForm.Response.Account creator;
        }


        @Data
        public static class FindAll {

            @ApiModelProperty(value="권한일련번호")
            private Long id;

            @ApiModelProperty(value="권한구분")
            private RoleType roleType;

            @ApiModelProperty(value="설명")
            private String description;

            @ApiModelProperty(value="등록일시")
            private LocalDateTime createdAt;

            @ApiModelProperty(value="생성자")
            private BaseForm.Response.Account creator;
        }
    }
}
