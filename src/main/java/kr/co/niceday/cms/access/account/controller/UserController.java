package kr.co.niceday.cms.access.account.controller;

import kr.co.niceday.cms.access.account.form.UserForm.Request;
import kr.co.niceday.cms.access.account.form.UserForm.Response;
import kr.co.niceday.cms.access.account.predicate.UserPredicate;
import kr.co.niceday.cms.access.account.service.AccountService;
import kr.co.niceday.cms.access.role.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

import static kr.co.niceday.cms.access.account.mapper.UserMapper.mapper;

/**
 * @since       2021.12.03
 * @author      preah
 * @description user controller
 **********************************************************************************************************************/
@Controller
@RequiredArgsConstructor
@RequestMapping("${property.context.path}")
public class UserController {

    private final AccountService accountService;
    private final RoleService    roleService;


    @GetMapping("/access/account/user/page")
    public String page(Model model, @ModelAttribute Request.Find find, @PageableDefault @SortDefault(sort="id", direction=Sort.Direction.DESC) Pageable pageable) {
        Page<Response.FindAll> page = accountService.getPage(UserPredicate.search(find), pageable).map(mapper::toFindAll);
        model.addAttribute("find", find);
        model.addAttribute("page", page);
        return "access/account/user/page";
    }

    @GetMapping("/access/account/user/detail")
    public String detail(Model model, @RequestParam Long id) {
        model.addAttribute("findOne", mapper.toFindOne(accountService.get(id)));
        return "access/account/user/detail";
    }

    @GetMapping("/access/account/user/add-form")
    public String add(Model model) {
        model.addAttribute(model.asMap().getOrDefault("add", Request.Add.builder().build()));
        model.addAttribute("roles", mapper.toFindOne(roleService.gets()));
        return "access/account/user/add-form";
    }

    @PostMapping("/access/account/user/add")
    public String add(@Valid @ModelAttribute Request.Add add) {
        accountService.add(mapper.toAccount(add));
        return "redirect:/access/account/user/page";
    }

    @GetMapping("/access/account/user/modify-form")
    public String modify(Model model, @RequestParam Long id) {
        model.addAttribute("modify", model.asMap().getOrDefault("modify", mapper.toFindOne(accountService.get(id))));
        model.addAttribute("roles", mapper.toFindOne(roleService.gets()));
        return "access/account/user/modify-form";
    }

    @PostMapping("/access/account/user/modify")
    public String modify(RedirectAttributes redirectAttributes, @Valid @ModelAttribute Request.Modify modify) {
        redirectAttributes.addAttribute("id", accountService.modify(mapper.toAccount(modify)).getId());
        return "redirect:/access/account/user/detail";
    }

    @PostMapping("/access/account/user/delete")
    public String delete(@RequestParam Long id) {
        accountService.remove(id);
        return "redirect:/access/account/user/page";
    }
}
