package kr.co.niceday.cms.access.resource.entity;

import kr.co.niceday.common.base.entity.Base;
import kr.co.niceday.common.engine.annotation.entity.Description;
import kr.co.niceday.cms.access.role.entity.Role;
import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * @since       2021.12.13
 * @author      preah
 * @description resource
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity
@Table(name="resource")
@Description("자원")
public class Resource extends Base {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Description("자원일련번호")
    @Column(nullable=false)
    private Long id;

    @Description("이름")
    @Column(nullable=false, length=500)
    private String name;

    @Description("패턴")
    @Column(nullable=false, length=100)
    private String pattern;

    @Description("설명")
    @Column(length=500)
    private String description;


    @Description("권한목록")
    @OneToMany(fetch=FetchType.LAZY)
    @JoinTable(name="role_resource", joinColumns=@JoinColumn(name="resource_id"), inverseJoinColumns=@JoinColumn(name="role_id"))
    private List<Role> roles;
}
