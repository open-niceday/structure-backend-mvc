package kr.co.niceday.cms.access.menu.controller;

import kr.co.niceday.cms.access.menu.form.MenuForm.Request;
import kr.co.niceday.cms.access.menu.service.MenuService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

import static kr.co.niceday.cms.access.menu.mapper.MenuMapper.mapper;

/**
 * @since       2021.12.03
 * @author      preah
 * @description menu controller
 **********************************************************************************************************************/
@Controller
@RequiredArgsConstructor
@RequestMapping("${property.context.path}")
public class MenuController {

    private final MenuService menuService;

    @GetMapping("/access/menu/tree")
    public String list(Model model) {
        model.addAttribute("findOne", mapper.toFindOne(menuService.getRoot()));
        return "access/menu/tree";
    }

    @PostMapping("/access/menu/add")
    public String add(@Valid @ModelAttribute Request.Add add) {
        menuService.add(mapper.toMenu(add));
        return "redirect:/access/menu/tree";
    }

    @GetMapping("/access/menu/add-form")
    public String add(Model model, @RequestParam("parent.id") Long parentId) {
        model.addAttribute(model.asMap().getOrDefault("add", Request.Add.builder().parent(Request.Add.Parent.builder().id(parentId).build()).build()));
        return "access/menu/add-form";
    }

    @GetMapping("/access/menu/detail")
    public String detail(Model model, @RequestParam Long id) {
        model.addAttribute("findOne", mapper.toFindOne(menuService.get(id)));
        return "access/menu/detail";
    }

    @GetMapping("/access/menu/modify-form")
    public String modify(Model model, @RequestParam Long id) {
        model.addAttribute("modify", model.asMap().getOrDefault("modify", mapper.toFindOne(menuService.get(id))));
        return "access/menu/modify-form";
    }

    @PostMapping("/access/menu/modify")
    public String modify(RedirectAttributes redirectAttributes, @Valid @ModelAttribute Request.Modify modify) {
        redirectAttributes.addAttribute("id", menuService.modify(mapper.toMenu(modify)).getId());
        return "redirect:/access/menu/detail";
    }

    @PostMapping("/access/menu/delete")
    public String delete(@RequestParam Long id) {
        menuService.remove(id);
        return "redirect:/access/menu/tree";
    }
}
