package kr.co.niceday.cms.access.account.service;

import com.querydsl.core.types.Predicate;
import kr.co.niceday.cms.access.account.entity.Account;
import kr.co.niceday.cms.access.account.repository.AccountRepository;
import kr.co.niceday.common.engine.exception.NoFoundException;
import kr.co.niceday.common.engine.helper.account.Principal;
import kr.co.niceday.common.security.exception.UserNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static kr.co.niceday.cms.access.account.mapper.UserMapper.mapper;

/**
 * @since       2021.09.23
 * @author      preah
 * @description account service
 **********************************************************************************************************************/
@Service
@Transactional
public class AccountService implements UserDetailsService {
    private final AccountRepository accountRepository;

    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String identifier) throws UsernameNotFoundException {

        return accountRepository.findByIdentifier(identifier)
                .map        (account -> new Principal(account))
                .orElseThrow(UserNotFoundException::new);
    }

    @Transactional(readOnly=true)
    public Page<Account> getPage(Predicate predicate, Pageable pageable) {
        return accountRepository.findAll(predicate, pageable);
    }

    @Transactional(readOnly=true)
    public Account get(Long id) {
        return accountRepository.findById(id).orElseThrow(NoFoundException::new);
    }

    public Account add(Account account) {
        return accountRepository.save(account.toBuilder()
                                             .password(passwordEncoder.encode(account.getPassword()))
                                             .build());
    }

    public Account modify(Account account) {
        return mapper.modify(account, get(account.getId()));
    }

    public void remove(Long id) {
        accountRepository.deleteById(id);
    }

    AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
        this.passwordEncoder   = new BCryptPasswordEncoder();
    }
}
