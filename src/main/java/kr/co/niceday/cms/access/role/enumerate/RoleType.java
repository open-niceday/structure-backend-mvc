package kr.co.niceday.cms.access.role.enumerate;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @since       2021.12.09
 * @author      preah
 * @description 권한 타입
 *               - ROLE_ADMIN     : 관리자
 *               - ROLE_USER      : 사용자
 *               - ROLE_ANONYMOUS : 익명사용자
 **********************************************************************************************************************/
@Getter
@AllArgsConstructor
public enum RoleType {

     ROLE_ADMIN    ("관리자")
    ,ROLE_USER     ("사용자")
    ,ROLE_ANONYMOUS("익명사용자")
    ,ROLE_TEST("TEST");

    private String description;
}
