package kr.co.niceday.cms.access.role.service;

import com.querydsl.core.types.Predicate;
import kr.co.niceday.cms.access.account.repository.AccountRepository;
import kr.co.niceday.cms.access.role.entity.Role;
import kr.co.niceday.cms.access.role.exception.RoleException;
import kr.co.niceday.cms.access.role.repository.RoleRepository;
import kr.co.niceday.common.engine.exception.NoFoundException;
import kr.co.niceday.common.engine.exception.common.ExceptionCode;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static kr.co.niceday.cms.access.role.mapper.RoleMapper.mapper;

/**
 * @since       2021.12.16
 * @author      preah
 * @description role service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class RoleService {

    private final RoleRepository    roleRepository;
    private final AccountRepository accountRepository;

    @Transactional(readOnly=true)
    public Page<Role> getPage(Predicate predicate, Pageable pageable) {
        return roleRepository.findAll(predicate, pageable);
    }

    @Transactional(readOnly=true)
    public Role get(Long id) {
        return roleRepository.findById(id).orElseThrow(NoFoundException::new);
    }

    @Transactional(readOnly=true)
    public List<Role> gets() {
        return roleRepository.findAll();
    }

    public Role add(Role role) {
        if(roleRepository.existsByRoleType(role.getRoleType())) {
            throw new RoleException(ExceptionCode.E00200002);
        }
        return roleRepository.save(role);
    }

    public Role modify(Role role) {

        List<Role> roles = roleRepository.findByRoleType(role.getRoleType())
                                         .orElse(Collections.emptyList());

        if(roles.stream()
                .filter  (r -> BooleanUtils.isFalse(r.getId().equals(role.getId())))
                .anyMatch(r -> r.getRoleType().equals(role.getRoleType()))) {
            throw new RoleException(ExceptionCode.E00200002);
        }
        return mapper.modify(role, get(role.getId()));
    }

    public void remove(Long id) {
        if(accountRepository.existsByRolesIn(Arrays.asList(this.get(id)))) {
            throw new RoleException(ExceptionCode.E00200001);
        }
        roleRepository.deleteById(id);
    }
}
