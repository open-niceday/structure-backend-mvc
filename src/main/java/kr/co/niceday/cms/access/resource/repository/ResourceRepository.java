package kr.co.niceday.cms.access.resource.repository;

import kr.co.niceday.cms.access.resource.entity.Resource;
import kr.co.niceday.cms.access.role.enumerate.RoleType;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @since       2021.09.23
 * @author      preah
 * @description resource repository
 **********************************************************************************************************************/
@Repository
public interface ResourceRepository extends JpaRepository<Resource, Long>, QuerydslPredicateExecutor<Resource> {

    @EntityGraph(attributePaths={"roles"})
    List findAllByRolesRoleTypeIn(List<RoleType> roleTypes);
}
