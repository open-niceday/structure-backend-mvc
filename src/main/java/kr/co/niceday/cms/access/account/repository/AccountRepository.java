package kr.co.niceday.cms.access.account.repository;

import kr.co.niceday.cms.access.account.entity.Account;
import kr.co.niceday.cms.access.role.entity.Role;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @since       2021.09.23
 * @author      preah
 * @description account repository
 **********************************************************************************************************************/
@Repository
public interface AccountRepository extends JpaRepository<Account, Long>, QuerydslPredicateExecutor<Account> {

    @EntityGraph(attributePaths={"roles"})
    Optional<Account> findByIdentifier(String identifier);

    Boolean existsByRolesIn(List<Role> roles);
}
