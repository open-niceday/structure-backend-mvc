package kr.co.niceday.cms.access.role.entity;

import kr.co.niceday.cms.access.menu.entity.Menu;
import kr.co.niceday.cms.access.resource.entity.Resource;
import kr.co.niceday.common.base.entity.Base;
import kr.co.niceday.common.engine.annotation.entity.Description;
import kr.co.niceday.cms.access.role.enumerate.RoleType;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @since       2021.12.09
 * @author      preah
 * @description role
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity
@Description("권한")
@Table(name="role")
public class Role extends Base {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Description("권한일련번호")
    @Column(nullable=false)
    private Long id;

    @Description("권한구분")
    @Enumerated(EnumType.STRING)
    @Column(nullable=false, length=30)
    private RoleType roleType;

    @Description("설명")
    @Column(nullable=false, length=500)
    private String description;


    @Description("자원목록")
    @OneToMany(fetch=FetchType.LAZY)
    @JoinTable(name="role_resource", joinColumns=@JoinColumn(name="role_id"), inverseJoinColumns=@JoinColumn(name="resource_id"))
    private List<Resource> resources;

    @Description("메뉴목록")
    @OneToMany(fetch=FetchType.LAZY)
    @JoinTable(name="role_menu", joinColumns=@JoinColumn(name="role_id"), inverseJoinColumns=@JoinColumn(name="menu_id"))
    private List<Menu> menus;
}
