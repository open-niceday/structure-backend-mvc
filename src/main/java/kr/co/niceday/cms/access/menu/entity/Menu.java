package kr.co.niceday.cms.access.menu.entity;

import kr.co.niceday.cms.access.role.entity.Role;
import kr.co.niceday.common.base.entity.Base;
import kr.co.niceday.common.engine.annotation.entity.Description;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @since       2021.12.20
 * @author      preah
 * @description menu
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity
@Table(name="menu")
@Description("메뉴")
public class Menu extends Base {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Description("메뉴일련번호")
    @Column(nullable=false)
    private Long id;

    @Description("이름")
    @Column(nullable=false, length=100)
    private String name;

    @Description("순서")
    @Column(nullable=false, length=10)
    private Integer sort;

    @Description("주소")
    @Column(nullable=false, length=200)
    private String url;

    @Description("사용여부")
    @Column(nullable=false)
    private Boolean useYn;


    @Description("부모-메뉴")
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="up_id")
    private Menu parent;

    @Description("하위-메뉴")
    @OneToMany(mappedBy="parent", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @OrderBy("sort asc")
    private List<Menu> children;


    @Description("권한목록")
    @OneToMany(fetch=FetchType.LAZY)
    @JoinTable(name="role_menu", joinColumns=@JoinColumn(name="menu_id"), inverseJoinColumns=@JoinColumn(name="role_id"))
    private List<Role> roles;
}
