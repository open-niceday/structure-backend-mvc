package kr.co.niceday.cms.access.role.exception;

import kr.co.niceday.common.engine.exception.common.ExceptionCode;
import org.springframework.http.HttpStatus;

/**
 * @since       2021.12.20
 * @author      preah
 * @description role exception
 **********************************************************************************************************************/
@SuppressWarnings("serial")
public class RoleException extends RuntimeException {

	public RoleException(){
		super(ExceptionCode.E00100006.name());
	}

	public RoleException(ExceptionCode exceptionCode, HttpStatus httpStatus){
		super(exceptionCode.name());
		code   = exceptionCode;
		status = httpStatus;
	}

	public RoleException(ExceptionCode exceptionCode, Exception exception){
		super(exceptionCode.name(), exception);
		code = exceptionCode;
	}

	public RoleException(ExceptionCode exceptionCode){
		super(exceptionCode.name());
		code   = exceptionCode;
	}

	public static ExceptionCode code   = ExceptionCode.E00100006;
	public static HttpStatus    status = HttpStatus.NOT_ACCEPTABLE;
}
