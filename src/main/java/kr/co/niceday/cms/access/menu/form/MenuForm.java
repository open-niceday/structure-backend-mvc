package kr.co.niceday.cms.access.menu.form;

import io.swagger.annotations.ApiModelProperty;
import kr.co.niceday.cms.access.role.enumerate.RoleType;
import kr.co.niceday.common.base.form.BaseForm;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @since       2021.12.23
 * @author      preah
 * @description menu form
 **********************************************************************************************************************/
public class MenuForm {

    public static class Request {

        @Getter
        @Setter
        @Builder(toBuilder=true)
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Add {

            @ApiModelProperty(value="이름", required=true)
            @NotBlank
            @Length(max=100)
            private String name;

            @ApiModelProperty(value="순서", required=true)
            @NotNull
            private Integer sort;

            @ApiModelProperty(value="주소")
            @Length(max=200)
            private String url;

            @ApiModelProperty(value="사용여부", required=true)
            @NotNull
            private Boolean useYn;

            @ApiModelProperty(value="부모-메뉴", required=true)
            @Valid
            @NotNull
            private Parent parent;

            @Getter
            @Setter
            @Builder(toBuilder=true)
            @NoArgsConstructor
            @AllArgsConstructor
            public static class Parent {

                @ApiModelProperty(value="메뉴일련번호", required=true)
                @NotNull
                private Long id;
            }
        }

        @Getter
        @Setter
        @Builder(toBuilder=true)
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Modify {

            @ApiModelProperty(value="메뉴일련번호", required=true)
            @NotNull
            private Long id;

            @ApiModelProperty(value="이름", required=true)
            @NotBlank
            @Length(max=100)
            private String name;

            @ApiModelProperty(value="순서", required=true)
            @NotNull
            private Integer sort;

            @ApiModelProperty(value="주소")
            @Length(max=200)
            private String url;

            @ApiModelProperty(value="사용여부", required=true)
            @NotNull
            private Boolean useYn;
        }
    }

    public static class Response {

        @Data
        public static class FindOne {

            @ApiModelProperty(value="메뉴일련번호")
            private Long id;

            @ApiModelProperty(value="이름")
            private String name;

            @ApiModelProperty(value="순서")
            private Integer sort;

            @ApiModelProperty(value="주소")
            private String url;

            @ApiModelProperty(value="사용여부")
            private Boolean useYn;
            
            @ApiModelProperty(value="권한목록")
            private List<Role> roles;

            @Data
            public static class Role {

                @ApiModelProperty(value="권한일련번호")
                private Long id;

                @ApiModelProperty(value="권한구분")
                private RoleType roleType;

                @ApiModelProperty(value="설명")
                private String description;

                @ApiModelProperty(value="등록일시")
                private LocalDateTime createdAt;

                @ApiModelProperty(value="생성자")
                private BaseForm.Response.Account creator;
            }

            @ApiModelProperty(value="메뉴-자식")
            private List<Child> children;

            @Data
            public static class Child {

                @ApiModelProperty(value="메뉴일련번호")
                private Long id;

                @ApiModelProperty(value="이름")
                private String name;

                @ApiModelProperty(value="순서")
                private Integer sort;

                @ApiModelProperty(value="주소")
                private String url;

                @ApiModelProperty(value="사용여부")
                private Boolean useYn;

                @ApiModelProperty(value="메뉴-부모")
                private Parent parent;

                @ApiModelProperty(value="메뉴-자식")
                private List<Child> children;

                @ApiModelProperty(value="등록일시")
                private LocalDateTime createdAt;

                @ApiModelProperty(value="생성자")
                private BaseForm.Response.Account creator;
            }

            @ApiModelProperty(value="메뉴-부모")
            private Parent parent;

            @Data
            public static class Parent {

                @ApiModelProperty(value="메뉴일련번호")
                private Long id;

                @ApiModelProperty(value="이름")
                private String name;

                @ApiModelProperty(value="순서")
                private Integer sort;

                @ApiModelProperty(value="주소")
                private String url;

                @ApiModelProperty(value="사용여부")
                private Boolean useYn;

                @ApiModelProperty(value="등록일시")
                private LocalDateTime createdAt;

                @ApiModelProperty(value="생성자")
                private BaseForm.Response.Account creator;
            }

            @ApiModelProperty(value="등록일시")
            private LocalDateTime createdAt;

            @ApiModelProperty(value="생성자")
            private BaseForm.Response.Account creator;
        }
    }
}
