package kr.co.niceday.cms.access.role.controller;

import kr.co.niceday.cms.access.menu.service.MenuService;
import kr.co.niceday.cms.access.resource.service.ResourceService;
import kr.co.niceday.cms.access.role.form.RoleForm.Request;
import kr.co.niceday.cms.access.role.form.RoleForm.Response;
import kr.co.niceday.cms.access.role.predicate.RolePredicate;
import kr.co.niceday.cms.access.role.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

import static kr.co.niceday.cms.access.role.mapper.RoleMapper.mapper;

/**
 * @since       2021.12.03
 * @author      preah
 * @description role controller
 **********************************************************************************************************************/
@Controller
@RequiredArgsConstructor
@RequestMapping("${property.context.path}")
public class RoleController {

    private final RoleService     roleService;
    private final ResourceService resourceService;
    private final MenuService     menuService;

    @GetMapping("/access/role/page")
    public String page(Model model, @ModelAttribute Request.Find find, @PageableDefault @SortDefault(sort="id", direction=Sort.Direction.DESC) Pageable pageable) {
        Page<Response.FindAll> page = roleService.getPage(RolePredicate.search(find), pageable).map(mapper::toFindAll);
        model.addAttribute("find", find);
        model.addAttribute("page", page);
        return "access/role/page";
    }

    @GetMapping("/access/role/detail")
    public String detail(Model model, @RequestParam Long id) {
        model.addAttribute("findOne",   mapper.toFindOne(roleService.get(id)));
        return "access/role/detail";
    }

    @GetMapping("/access/role/add-form")
    public String add(Model model) {
        model.addAttribute(model.asMap().getOrDefault("add", Request.Add.builder().build()));
        model.addAttribute("resources", mapper.toFindOne(resourceService.gets()));
        model.addAttribute("menu",      mapper.toFindOne(menuService.getRoot()));
        return "access/role/add-form";
    }

    @PostMapping("/access/role/add")
    public String add(@Valid @ModelAttribute Request.Add add) {
        roleService.add(mapper.toRole(add));
        return "redirect:/access/role/page";
    }

    @GetMapping("/access/role/modify-form")
    public String modify(Model model, @RequestParam Long id) {
        model.addAttribute("modify", model.asMap().getOrDefault("modify", mapper.toFindOne(roleService.get(id))));
        model.addAttribute("resources", mapper.toFindOne(resourceService.gets()));
        model.addAttribute("menu",      mapper.toFindOne(menuService.getRoot()));
        return "access/role/modify-form";
    }

    @PostMapping("/access/role/modify")
    public String modify(RedirectAttributes redirectAttributes, @Valid @ModelAttribute Request.Modify modify) {
        redirectAttributes.addAttribute("id", roleService.modify(mapper.toRole(modify)).getId());
        return "redirect:/access/role/detail";
    }

    @PostMapping("/access/role/delete")
    public String delete(@RequestParam Long id) {
        roleService.remove(id);
        return "redirect:/access/role/page";
    }
}
