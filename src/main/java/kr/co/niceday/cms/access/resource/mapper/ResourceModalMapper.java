package kr.co.niceday.cms.access.resource.mapper;

import kr.co.niceday.cms.access.resource.entity.Resource;
import kr.co.niceday.cms.access.resource.form.ResourceModalForm.SearchPage;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @since       2021.12.13
 * @author      preah
 * @description resource modal mapper
 **********************************************************************************************************************/
@Mapper
public interface ResourceModalMapper {

    ResourceModalMapper mapper = Mappers.getMapper(ResourceModalMapper.class);

    SearchPage.Response.FindAll toFindAll(Resource entity);
}
