package kr.co.niceday.cms.access.resource.controller;

import kr.co.niceday.cms.access.resource.form.ResourceModalForm.SearchPage;
import kr.co.niceday.cms.access.resource.predicate.ResourceModalPredicate;
import kr.co.niceday.cms.access.resource.service.ResourceService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import static kr.co.niceday.cms.access.resource.mapper.ResourceModalMapper.mapper;

/**
 * @since       2021.12.03
 * @author      preah
 * @description resource modal controller
 **********************************************************************************************************************/
@Controller
@RequiredArgsConstructor
@RequestMapping("${property.context.path}")
public class ResourceModalController {

    private final ResourceService resourceService;

    @GetMapping("/access/resource/modal/search-page")
    public String searchPage(Model model, @ModelAttribute SearchPage.Request.Find find, @PageableDefault @SortDefault(sort="id", direction=Sort.Direction.DESC) Pageable pageable) {
        Page<SearchPage.Response.FindAll> page = resourceService.getPage(ResourceModalPredicate.search(find), pageable).map(mapper::toFindAll);
        model.addAttribute("find", find);
        model.addAttribute("page", page);
        return "access/resource/modal/search-page";
    }
}
