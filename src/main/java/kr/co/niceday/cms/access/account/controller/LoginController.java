package kr.co.niceday.cms.access.account.controller;

import kr.co.niceday.common.engine.exception.common.ExceptionCode;
import kr.co.niceday.common.engine.helper.exception.ExceptionHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @since       2021.12.03
 * @author      preah
 * @description login controller
 **********************************************************************************************************************/
@Controller
@RequiredArgsConstructor
@RequestMapping("${property.context.path}")
public class LoginController {

    @GetMapping("/access/account/login/form")
    public String form() {
        return "access/account/login/form";
    }

    @PostMapping("/access/account/login/failure")
    public String failure(HttpServletRequest request, Model model) {
        ExceptionCode exceptionCode = (ExceptionCode) request.getAttribute("exceptionCode");
        model.addAttribute("exception", ExceptionHelper.getException(exceptionCode));
        return "access/account/login/form";
    }
}
