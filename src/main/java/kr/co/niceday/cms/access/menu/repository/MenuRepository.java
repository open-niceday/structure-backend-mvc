package kr.co.niceday.cms.access.menu.repository;

import kr.co.niceday.cms.access.menu.entity.Menu;
import kr.co.niceday.cms.access.role.enumerate.RoleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @since       2021.09.23
 * @author      preah
 * @description menu repository
 **********************************************************************************************************************/
@Repository
public interface MenuRepository extends JpaRepository<Menu, Long>, QuerydslPredicateExecutor<Menu> {

    Optional<Menu> findByParent(Menu menu);

    List<Menu> findByUseYnAndRolesRoleTypeIn(Boolean useYn, Set<RoleType> roleTypes);
}
