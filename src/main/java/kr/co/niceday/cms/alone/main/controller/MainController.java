package kr.co.niceday.cms.alone.main.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("${property.context.path}")
public class MainController {

    @GetMapping({"/"})
    public String root() {
        return "redirect:/alone/main/dash-board";
    }

    @GetMapping("/alone/main/dash-board")
    public String dashBoardView(ModelAndView modelAndView) {
        return "alone/main/dash-board";
    }
}
