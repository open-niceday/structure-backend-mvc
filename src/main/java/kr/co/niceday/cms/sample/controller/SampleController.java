package kr.co.niceday.cms.sample.controller;

import kr.co.niceday.cms.sample.form.SchoolForm.Request;
import kr.co.niceday.cms.sample.form.SchoolForm.Response;
import kr.co.niceday.cms.sample.predicate.SchoolPredicate;
import kr.co.niceday.cms.sample.service.SchoolService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

import static kr.co.niceday.cms.sample.mapper.SchoolMapper.mapper;

/**
 * @since       2021.12.03
 * @author      preah
 * @description sample controller
 **********************************************************************************************************************/
@Controller
@RequiredArgsConstructor
@RequestMapping("${property.context.path}")
public class SampleController {

    private final SchoolService schoolService;

    @GetMapping("/sample/page")
    public String page(Model model, @ModelAttribute Request.Find find, @PageableDefault @SortDefault(sort="id", direction=Sort.Direction.DESC) Pageable pageable) {
        Page<Response.FindAll> page = schoolService.getPage(SchoolPredicate.search(find), pageable).map(mapper::toFindAll);
        model.addAttribute("find", find);
        model.addAttribute("page", page);
        return "sample/page";
    }

    @GetMapping("/sample/detail")
    public String detail(Model model, @RequestParam Long id) {
        model.addAttribute("findOne", mapper.toFindOne(schoolService.get(id)));
        return "sample/detail";
    }

    @GetMapping("/sample/add-form")
    public String add(Model model) {
        model.addAttribute(model.asMap().getOrDefault("add", Request.Add.builder().build()));
        return "sample/add-form";
    }

    @PostMapping("/sample/add")
    public String add(@Valid @ModelAttribute Request.Add add) {
        schoolService.add(mapper.toSchool(add));
        return "redirect:/sample/page";
    }

    @GetMapping("/sample/modify-form")
    public String modify(Model model, @RequestParam Long id) {
        model.addAttribute("modify", model.asMap().getOrDefault("modify", mapper.toFindOne(schoolService.get(id))));
        return "sample/modify-form";
    }

    @PostMapping("/sample/modify")
    public String modify(RedirectAttributes redirectAttributes, @Valid @ModelAttribute Request.Modify modify) {
        redirectAttributes.addAttribute("id", schoolService.modify(mapper.toSchool(modify)).getId());
        return "redirect:/sample/detail";
    }

    @PostMapping("/sample/delete")
    public String delete(@RequestParam Long id) {
        schoolService.remove(id);
        return "redirect:/sample/page";
    }
}