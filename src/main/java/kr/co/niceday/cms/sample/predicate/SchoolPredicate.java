package kr.co.niceday.cms.sample.predicate;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import kr.co.niceday.cms.sample.entity.QSchool;
import kr.co.niceday.cms.sample.form.SchoolForm.Request;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * @since       2021.09.23
 * @author      preah
 * @description school predicate
 **********************************************************************************************************************/
public class SchoolPredicate {

    public static Predicate search(Request.Find find) {
        QSchool school  = QSchool.school;
        BooleanBuilder builder = new BooleanBuilder();

        if(Objects.nonNull(find)) {
            if(StringUtils.isNotBlank(find.getName())){
                builder.and(school.name.contains(find.getName()));
            }
            if(Objects.nonNull(find.getSchoolType())) {
                builder.and(school.schoolType.eq(find.getSchoolType()));
            }
        }
        return builder;
    }
}
