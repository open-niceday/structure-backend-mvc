package kr.co.niceday.cms.sample.form;

import io.swagger.annotations.ApiModelProperty;
import kr.co.niceday.cms.sample.enumerate.SchoolType;
import kr.co.niceday.common.base.form.BaseForm;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @since       2021.09.23
 * @author      preah
 * @description school form
 **********************************************************************************************************************/
public class SchoolForm {

    public static class Request {

        @Getter
        @Setter
        @Builder(toBuilder=true)
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Find {

            @ApiModelProperty(value="이름")
            private String name;

            @ApiModelProperty(value="학교구분")
            private SchoolType schoolType;
        }

        @Getter
        @Setter
        @Builder(toBuilder=true)
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Add {

            @ApiModelProperty(value="이름", required=true)
            @NotBlank
            private String name;

            @ApiModelProperty(value="학교구분", required=true)
            @NotNull
            private SchoolType schoolType;
        }

        @Getter
        @Setter
        @Builder(toBuilder=true)
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Modify{

            @ApiModelProperty(value="학교일련번호", required=true)
            @NotNull
            private Long id;

            @ApiModelProperty(value="이름", required=true)
            @NotBlank
            private String name;

            @ApiModelProperty(value="학교구분", required=true)
            @NotNull
            private SchoolType schoolType;
        }
    }

    public static class Response {

        @Data
        public static class FindOne {

            @ApiModelProperty(value="학교일련번호")
            private Long id;

            @ApiModelProperty(value="이름")
            private String name;

            @ApiModelProperty(value="학교구분")
            private SchoolType schoolType;

            @ApiModelProperty(value="등록일시")
            private LocalDateTime createdAt;

            @ApiModelProperty(value="생성자")
            private BaseForm.Response.Account creator;
        }

        @Data
        public static class FindAll {

            @ApiModelProperty(value="학교일련번호")
            private Long id;

            @ApiModelProperty(value="이름")
            private String name;

            @ApiModelProperty(value="학교구분")
            private SchoolType schoolType;

            @ApiModelProperty(value="등록일시")
            private LocalDateTime createdAt;

            @ApiModelProperty(value="생성자")
            private BaseForm.Response.Account creator;
        }
    }
}
