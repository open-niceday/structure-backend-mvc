package kr.co.niceday.cms.sample.entity;

import kr.co.niceday.common.base.entity.Base;
import kr.co.niceday.common.engine.annotation.entity.Description;
import kr.co.niceday.cms.sample.enumerate.SchoolType;
import lombok.*;

import javax.persistence.*;

/**
 * @since       2021.09.23
 * @author      preah
 * @description school
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity
@Description("학교")
public class School extends Base {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Description("학교일련번호")
    @Column(nullable=false)
    private Long id;

    @Description("이름")
    @Column(nullable=false)
    private String name;

    @Description("학교구분")
    @Enumerated(EnumType.STRING)
    @Column(nullable=false, length=30)
    private SchoolType schoolType;
}
