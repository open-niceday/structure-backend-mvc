package kr.co.niceday.cms.sample.mapper;

import kr.co.niceday.cms.sample.entity.School;
import kr.co.niceday.cms.sample.form.SchoolForm.Request;
import kr.co.niceday.cms.sample.form.SchoolForm.Response;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

/**
 * @since       2021.09.23
 * @author      preah
 * @description school mapper
 **********************************************************************************************************************/
@Mapper
public interface SchoolMapper {

    SchoolMapper mapper = Mappers.getMapper(SchoolMapper.class);

    School toSchool(Request.Add form);
    School toSchool(Request.Modify form);

    Response.FindAll toFindAll(School entity);
    Response.FindOne toFindOne(School entity);

    School modify(School source, @MappingTarget School target);
}
