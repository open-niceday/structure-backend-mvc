package kr.co.niceday.cms.sample.repository;

import kr.co.niceday.cms.sample.entity.School;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * @since       2021.09.23
 * @author      preah
 * @description school repository
 **********************************************************************************************************************/
@Repository
public interface SchoolRepository extends JpaRepository<School, Long>, QuerydslPredicateExecutor<School> {
}
