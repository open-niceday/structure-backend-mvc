package kr.co.niceday.cms.sample.service;

import com.querydsl.core.types.Predicate;
import kr.co.niceday.common.engine.exception.NoFoundException;
import kr.co.niceday.cms.sample.entity.School;
import kr.co.niceday.cms.sample.repository.SchoolRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static kr.co.niceday.cms.sample.mapper.SchoolMapper.mapper;

/**
 * @since       2021.09.23
 * @author      preah
 * @description school service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class SchoolService {

    private final SchoolRepository schoolRepository;

    @Transactional(readOnly=true)
    public Page<School> getPage(Predicate predicate, Pageable pageable) {
        return schoolRepository.findAll(predicate, pageable);
    }

    @Transactional(readOnly=true)
    public School get(Long id) {
        return schoolRepository.findById(id).orElseThrow(NoFoundException::new);
    }

    public School add(School school) {
        return schoolRepository.save(school);
    }

    public School modify(School school) {
        return mapper.modify(school, get(school.getId()));
    }

    public void remove(Long id) {
        schoolRepository.deleteById(id);
    }
}
