package kr.co.niceday.cms.sample.enumerate;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @since       2021.09.23
 * @author      preah
 * @description school type
 **********************************************************************************************************************/
@Getter
@AllArgsConstructor
public enum SchoolType {

     ELEMENTARY_SCHOOL("초등학교")
    ,MIDDLE_SCHOOL    ("중학교")
    ,HIGH_SCHOOL      ("고등학교")
    ,UNIVERSITY       ("대학교")
    ,GRADUATE_SCHOOL  ("대학원")
    ,ETC              ("기타");

     private String description;
}
