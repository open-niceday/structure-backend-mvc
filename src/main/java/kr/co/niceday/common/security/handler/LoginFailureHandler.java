package kr.co.niceday.common.security.handler;

import com.google.common.collect.ImmutableMap;
import kr.co.niceday.common.engine.exception.common.ExceptionCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @since       2021.12.22
 * @author      preah
 * @description login failure handler
 ************************************************************************************************************************/
@Slf4j
public class LoginFailureHandler implements AuthenticationFailureHandler {

    private final ImmutableMap EXCEPTION = ImmutableMap.builder()
            .put(InternalAuthenticationServiceException.class, ExceptionCode.E00100102)
            .put(BadCredentialsException.class,                ExceptionCode.E00100103)
            .build();

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        request.setAttribute("exceptionCode", EXCEPTION.getOrDefault(exception.getClass(), ExceptionCode.E00100101));
        request.getRequestDispatcher("/access/account/login/failure").forward(request, response);
    }
}
