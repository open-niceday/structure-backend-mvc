package kr.co.niceday.common.security.auditor;

import kr.co.niceday.cms.access.account.entity.Account;
import kr.co.niceday.common.engine.helper.account.PrincipalHelper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * @since       2021.03.30
 * @author      preah
 * @description auditor
 **********************************************************************************************************************/
@Component
@RequiredArgsConstructor
public class Auditor implements AuditorAware<Account> {

    @Override
    public Optional<Account> getCurrentAuditor() {
        Account account = PrincipalHelper.getAccount();
        return ObjectUtils.allNotNull(account) ? Optional.of(account) : Optional.empty();
    }
}