package kr.co.niceday.common.security.config;

import kr.co.niceday.cms.access.account.service.AccountService;
import kr.co.niceday.cms.access.resource.entity.Resource;
import kr.co.niceday.cms.access.resource.repository.ResourceRepository;
import kr.co.niceday.cms.access.role.enumerate.RoleType;
import kr.co.niceday.common.security.handler.LoginFailureHandler;
import kr.co.niceday.common.security.handler.LoginSuccessHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.firewall.HttpFirewall;

import java.util.Arrays;
import java.util.List;

/**
 * @since       2021.12.13
 * @author      preah
 * @description security configuration
 **********************************************************************************************************************/
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(securedEnabled=true, prePostEnabled=true)
@Order(SecurityProperties.BASIC_AUTH_ORDER)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final AccountService accountService;

    private final ResourceRepository resourceRepository;


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
        auth.eraseCredentials  (false)
            .userDetailsService(accountService)
            .passwordEncoder   (new BCryptPasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

        List<Resource> resources = resourceRepository.findAllByRolesRoleTypeIn(Arrays.asList(RoleType.values()));

        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry=httpSecurity.authorizeRequests();

        for(Resource resource : resources) {
            registry.antMatchers    (resource.getPattern())
                    .hasAnyAuthority(resource.getRoles().stream().map(role -> role.getRoleType().name()).toArray(String[]::new));
        }

        registry.anyRequest().authenticated();

        httpSecurity
                .headers()
                     .frameOptions()
                     .sameOrigin()
                     .and()
                .csrf().disable()
                    .formLogin()
                        .loginPage          ("/access/account/login/form")
                        .loginProcessingUrl ("/access/account/login/process")
                        .usernameParameter  ("identifier")
                        .passwordParameter  ("password")
                        .successHandler     (new LoginSuccessHandler())
                        .failureHandler     (new LoginFailureHandler())
                        .permitAll()
                        .and()
                    .exceptionHandling()
                        .accessDeniedPage   ("/common/error/403")
                        .and()
                    .logout()
                        .logoutUrl          ("/access/account/login/out")
                        .logoutSuccessUrl   ("/access/account/login/form")
                        .deleteCookies      ("JSESSIONID")
                        .clearAuthentication(true)
                    .permitAll();
    }

    @Override
    public void configure(WebSecurity webSecurity) throws Exception {
        webSecurity.ignoring()
                   .requestMatchers(PathRequest.toStaticResources().atCommonLocations())
                   .antMatchers("/font/**");
        webSecurity.httpFirewall(defaultHttpFirewall());
    }

    @Bean
    public HttpFirewall defaultHttpFirewall() {
        return new DefaultHttpFirewall();
    }

//    @Bean
//    public PasswordEncoder getEncoder() {
//        return new BCryptPasswordEncoder();
//    }
}
