package kr.co.niceday.common.security.exception;

import kr.co.niceday.common.engine.exception.common.ExceptionCode;
import org.springframework.http.HttpStatus;

/**
 * @since       2021.12.20
 * @author      preah
 * @description user not found exception
 **********************************************************************************************************************/
@SuppressWarnings("serial")
public class UserNotFoundException extends RuntimeException {

	public UserNotFoundException(){
		super(ExceptionCode.E00100101.name());
	}

	public UserNotFoundException(ExceptionCode exceptionCode, HttpStatus httpStatus){
		super(exceptionCode.name());
		code   = exceptionCode;
		status = httpStatus;
	}

	public UserNotFoundException(ExceptionCode exceptionCode, Exception exception){
		super(exceptionCode.name(), exception);
		code = exceptionCode;
	}
	public UserNotFoundException(ExceptionCode exceptionCode){
		super(exceptionCode.name());
		code   = exceptionCode;
	}
	
	public static ExceptionCode code   = ExceptionCode.E00100101;
	public static HttpStatus    status = HttpStatus.NOT_ACCEPTABLE;
}
