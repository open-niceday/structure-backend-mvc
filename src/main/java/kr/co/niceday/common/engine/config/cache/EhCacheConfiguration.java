package kr.co.niceday.common.engine.config.cache;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

/**
 * @since       2022.01.10
 * @author      preah
 * @description ehcache configuration
 **********************************************************************************************************************/
@Configuration
@EnableCaching
public class EhCacheConfiguration {
 
}