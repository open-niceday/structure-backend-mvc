package kr.co.niceday.common.engine.exception;

import kr.co.niceday.common.engine.exception.common.ExceptionCode;
import org.springframework.http.HttpStatus;

/**
 * @since       2021.12.20
 * @author      preah
 * @description not found exception
 **********************************************************************************************************************/
public class NoFoundException extends RuntimeException {

    public NoFoundException() {
        super(ExceptionCode.E00100002.name());
    }

    public NoFoundException(ExceptionCode exceptionCode, HttpStatus httpStatus){
        super(exceptionCode.name());
        code   = exceptionCode;
        status = httpStatus;
    }

    public NoFoundException(ExceptionCode exceptionCode, Exception exception){
        super(exceptionCode.name(), exception);
        code = exceptionCode;
    }
    public NoFoundException(ExceptionCode exceptionCode){
        super(exceptionCode.name());
        code   = exceptionCode;
    }
    

    public static ExceptionCode code    = ExceptionCode.E00100002;
    public static HttpStatus status     = HttpStatus.NO_CONTENT;
}

