package kr.co.niceday.common.engine.exception.common;

import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * @since       2021.12.06
 * @author      preah
 * @description exception page
 **********************************************************************************************************************/
@Controller
@RequestMapping("${property.context.path}")
public class ExceptionPage implements ErrorController {

    private final ImmutableSet<HttpStatus> ERROR_PAGES = ImmutableSet.<HttpStatus>builder()
                                                                     .add(HttpStatus.FORBIDDEN)
                                                                     .add(HttpStatus.NO_CONTENT)
                                                                     .add(HttpStatus.NOT_FOUND)
                                                                     .add(HttpStatus.INTERNAL_SERVER_ERROR)
                                                                     .build();

    @RequestMapping("/common/error")
    public String handleError(HttpServletRequest request) {
        return  StringUtils.join("/common/error/",
                Optional.ofNullable(request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE))
                        .map(status -> ERROR_PAGES.stream()
                                                  .filter(httpStatus -> httpStatus.value() == Integer.parseInt(status.toString()))
                                                  .findAny()
                                                  .orElse(HttpStatus.INTERNAL_SERVER_ERROR).value()).orElse(HttpStatus.INTERNAL_SERVER_ERROR.value()));
    }

    @RequestMapping("/common/error/403")
    public String error403() {
        return StringUtils.join("/common/error/", HttpStatus.FORBIDDEN.value());
    }

    @RequestMapping("/common/error/204")
    public String error204() {
        return StringUtils.join("/common/error/", HttpStatus.NO_CONTENT.value());
    }

    @RequestMapping("/common/error/404")
    public String error404() {
        return StringUtils.join("/common/error/", HttpStatus.NOT_FOUND.value());
    }

    @RequestMapping("/common/error/500")
    public String error500() {
        return StringUtils.join("/common/error/", HttpStatus.INTERNAL_SERVER_ERROR.value());
    }
}