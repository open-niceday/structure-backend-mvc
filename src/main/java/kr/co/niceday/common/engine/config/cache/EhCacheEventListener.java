package kr.co.niceday.common.engine.config.cache;

import lombok.extern.slf4j.Slf4j;
import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;

/**
 * @since       2022.01.10
 * @author      preah
 * @description ehcache event listener
 **********************************************************************************************************************/
@Slf4j
public class EhCacheEventListener implements CacheEventListener<Object, Object> {

    @Override
    public void onEvent(CacheEvent event) {
        log.debug("[{}-{}]{}", event.getType(), event.getKey(), event.getNewValue());
    }
}