package kr.co.niceday.common.engine.aop;

import kr.co.niceday.cms.access.account.adapter.AccountAdapter;
import kr.co.niceday.cms.access.account.form.AccountForm;
import kr.co.niceday.cms.access.role.entity.Role;
import kr.co.niceday.common.engine.helper.account.Principal;
import kr.co.niceday.common.engine.helper.account.PrincipalHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @since       2022.01.20
 * @author      preah
 * @description model appender
 **********************************************************************************************************************/
@ControllerAdvice
@RequiredArgsConstructor
public class ModelAppender {

    private final AccountAdapter menuAdapter;

    @ModelAttribute
    public void addAttributes(Model model) {
        Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication()).ifPresent(authentication -> {
            if(authentication.getPrincipal().getClass().equals(Principal.class)) {
                 AccountForm.Response.Sidebar sidebar =  menuAdapter.getSidebar(PrincipalHelper.getAccount().getRoles().stream().map(Role::getRoleType).collect(Collectors.toSet()));
                 model.addAttribute("sidebar", sidebar);
            }
        });
    }
}
