package kr.co.niceday.common.engine.aop;

import kr.co.niceday.cms.access.menu.exception.MenuException;
import kr.co.niceday.cms.access.resource.exception.ResourceException;
import kr.co.niceday.cms.access.role.exception.RoleException;
import kr.co.niceday.common.engine.constant.Constant;
import kr.co.niceday.common.engine.exception.NoFoundException;
import kr.co.niceday.common.engine.exception.common.ExceptionCode;
import kr.co.niceday.common.engine.helper.context.RequestHelper;
import kr.co.niceday.common.engine.helper.exception.ExceptionHelper;
import kr.co.niceday.common.security.exception.UserNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;


/**
 * @since       2021.12.06
 * @author      preah
 * @description exception handler(reference site : http://onecellboy.tistory.com/346)
 *              ref https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html#mvc-ann-modelattrib-method-args
 *              https://luvstudy.tistory.com/74
 **********************************************************************************************************************/
@ControllerAdvice
@Slf4j
public class ExceptionsHandler {

    @ExceptionHandler(BindException.class)
    protected String handleBindException(BindException exception, RedirectAttributes attributes) {
        log.debug(exception.getMessage());
        BindingResult bindingResult = exception.getBindingResult();
        attributes.addFlashAttribute(bindingResult.getObjectName(), bindingResult.getTarget());
        attributes.addFlashAttribute(StringUtils.join(BindingResult.class.getTypeName(), Constant.String.DOT, bindingResult.getObjectName()), bindingResult);
        return this.refererUrl();
    }

    @ExceptionHandler({UserNotFoundException.class, UsernameNotFoundException.class})
    public String handleUserNotFoundException(UserNotFoundException exception, RedirectAttributes attributes) {
        log.debug(exception.getMessage());
        return this.redirectUrl(exception.code, attributes);
    }

    @ExceptionHandler(NoFoundException.class)
    public String handleNoFoundException(NoFoundException exception, RedirectAttributes attributes) {
        log.debug(exception.getMessage());
        return this.redirectUrl(exception.code, attributes);
    }

    @ExceptionHandler(RoleException.class)
    public String handleRoleException(RoleException exception, RedirectAttributes attributes) {
        log.debug(exception.getMessage());
        return this.redirectUrl(exception.code, attributes);
    }


    @ExceptionHandler(ResourceException.class)
    public String handleResourceException(ResourceException exception, RedirectAttributes attributes) {
        log.debug(exception.getMessage());
        return this.redirectUrl(exception.code, attributes);
    }


    @ExceptionHandler(MenuException.class)
    public String handleMenuException(MenuException exception, RedirectAttributes attributes) {
        log.debug(exception.getMessage());
        return this.redirectUrl(exception.code, attributes);
    }

    private String redirectUrl(ExceptionCode exceptionCode, RedirectAttributes attributes) {
        attributes.addFlashAttribute("exception", ExceptionHelper.getException(exceptionCode));
        return this.refererUrl();
    }

    private String refererUrl() {
        return StringUtils.join("redirect:/",
                Optional.ofNullable(RequestHelper.getRequest().getHeader("Referer"))
                        .map(url -> (StringUtils.split(StringUtils.removePattern(url, "^http(s)?://"), "/", Constant.Integer.TWO))[Constant.Integer.ONE])
                        .orElse(StringUtils.join("index")));
    }
}
