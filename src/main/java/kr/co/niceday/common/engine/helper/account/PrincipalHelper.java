package kr.co.niceday.common.engine.helper.account;

import kr.co.niceday.cms.access.account.entity.Account;
import kr.co.niceday.common.security.exception.UserNotFoundException;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * @since       2020.03.06
 * @author      lucas
 * @description principal helper
 **********************************************************************************************************************/
@Component
public class PrincipalHelper {

    public static Account getAccount() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(!ObjectUtils.allNotNull(authentication) || BooleanUtils.isFalse(authentication.isAuthenticated()) || authentication instanceof AnonymousAuthenticationToken){
            throw new UserNotFoundException();
        }

        return ((Principal)authentication.getPrincipal()).getAccount();
    }
}