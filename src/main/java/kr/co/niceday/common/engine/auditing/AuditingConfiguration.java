package kr.co.niceday.common.engine.auditing;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * @since       2021.12.02
 * @author      preah
 * @description auditing configuration
 **********************************************************************************************************************/
@Configuration
@EnableJpaAuditing
public class AuditingConfiguration {

}
