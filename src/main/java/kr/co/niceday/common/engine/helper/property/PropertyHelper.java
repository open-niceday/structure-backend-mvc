package kr.co.niceday.common.engine.helper.property;

import kr.co.niceday.common.engine.config.properties.MybatisConfiguration;
import kr.co.niceday.common.engine.config.properties.PropertiesConfiguration;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**   
 * @since       2018.10.03
 * @author      lucas
 * @description property helper 
 **********************************************************************************************************************/
@Component
public class PropertyHelper {
	
	@Autowired
    private PropertyHelper(PropertiesConfiguration propertiesConfiguration, MybatisConfiguration mybatisConfiguration) {
        PropertyHelper.property = propertiesConfiguration;
        PropertyHelper.mybatis  = mybatisConfiguration;
    }
	
	@Getter
	private static PropertiesConfiguration property = null;

    @Getter
    private static MybatisConfiguration mybatis = null;
}