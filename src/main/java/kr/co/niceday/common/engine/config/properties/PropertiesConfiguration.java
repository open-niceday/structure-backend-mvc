package kr.co.niceday.common.engine.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**   
 * @since       2018.10.03
 * @author      lucas
 * @description properties configuration
 **********************************************************************************************************************/
@Data
@Component
@ConfigurationProperties(prefix="property")
public class PropertiesConfiguration {

	private String name = null;

	@Data
	public static class Api {
		private String endPoint = null;
	}
	
	@Data
	public static class Swagger {
		private Info    info    = null;
		private Contact contact = null;
		
		@Data
		public static class Info {
			private String title   = null;
			private String desc    = null;
			private String version = null;
		}
		
		@Data
		public static class Contact {
			private String name  = null;
			private String url   = null;
			private String email = null;
		}
	}

	@Data
	public static class Attach {
		private String storeLocation = null;
	}

	@Data
	public static class Integrate {
		private Am am = null;

		@Data
		public static class Am {
			private String       endPoint    = null;
			private String       systemId    = null;
			private String       credit      = null;
			private String       denyAll     = null;
			private List<String> allowGrades = null;
		}
	}

	private Api       api       = null;
	private Swagger   swagger   = null;
	private Attach    attach    = null;
	private Integrate integrate = null;
}
