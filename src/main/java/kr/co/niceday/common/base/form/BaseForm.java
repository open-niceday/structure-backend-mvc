package kr.co.niceday.common.base.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @since       2022.01.20
 * @author      preah
 * @description base form
 **********************************************************************************************************************/
public class BaseForm {

    public static class Response {

        @Data
        public static class Account {

            @ApiModelProperty(value="사용자일련번호")
            private Long id;

            @ApiModelProperty(value="아이디")
            private String identifier;

            @ApiModelProperty(value="이름")
            private String name;

            @ApiModelProperty(value="이메일")
            private String email;
        }
    }
}
