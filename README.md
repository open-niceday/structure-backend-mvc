# 백엔드 개발 기준(structure-backend-mvc)
* structure-backend-mvc 개발을 위한 기본 개발환경을 제공한다.

# 이력
1. v0.0.1-SNAPSHOT
    *  2021.03.09
        * 최초 작성

# 구성

### 개발 환경
* mac (high sierra+), Linux, Windows
* IntelliJ IDE 2018.2.5+ 또는 Spring Tool Suite 4.0.0+
* Source tree
* Apache Maven
* adoptOpenJDK 1.8.0_275

### 라이브러리
    * spring boot 2.0.7.RELEASE
        * spring framework 5.0.9.RELEASE
        * spring rest docs
        * spring test(scope:test)
        * spring dev-tools(scope:runtime)
        * embedded tomcat 8.5.34(scope:provided)
        * hikaricp 2.7.9
        * jackson 2.9.6
    * flyway 5.0.7
    * lombok 1.18.6
    * commons-lang3 3.7
    * commons-pool2 2.5.0
    * commons-io 2.5
    * mybatis 1.3.2
    * mapstruct 1.2.0.Final
    * log4jdbc 1.16(profiles:default)
> 더 자세한 내용은 /{local}/repository/org/springframework/boot/spring-boot-dependencies/2.0.7.RELEASE/spring-boot-dependencies-2.0.7.RELEASE.pom dependency

### Presentation 라이브러리
    * Jquery 1.12.4
    * Toastr 2.1.2
    * momentJs 2.29.1
    * bootstrap 4.2.1
    * handlebars 4.7.6
    * momentjs 2.29.1

## 엔드 포인트
    - package : kr.co.niceday.cms


## 패키지 구조
| 패키지      | 설명        |
| --------  |  --------  |
| kr.co.niceday.cms.common | 공통 패키지(비니지스 로직이 포함되지 않는 상위 영역 ex. aop, config..) |
| integrate | 통합 공통 영역 (로그인 및 메인 사이트 접근을 위한 영역, 개별 프로젝트와는 별개) |
| sample    | 샘플 패키지(기본 CRUD)|
| common    | 비지니스 공통 영역|
| am        | AM 계정 연동 영역|
| alone     | 독립적 실행 영역|
| basis     | 비지니스 메인 영역 |

## 기능별 공통 패키지 구조
| 패키지       |        설명 |
|  --------  |  --------  |
| sample.controller |컨트롤러 영역(Controller)|
| sample.entity     |엔티티 영역(Entity)|
| sample.enumerate  |열거형 영역(Enum)|
| sample.form       |폼 영역(Dto)|
| sample.mapper     |매퍼 영역(Mapper)|
| sample.repository |레파지토리 영역(Repository)|
| sample.service    |서비스 영역(Service)|

## 기능별 공통 테스트 패키지 구조
| 패키지 | 설명 |
|  --------  |  --------                |
| sample   | 테스트 케이스 영역(Test, Helper) |

---
## 요청 주소 디자인
### 페이지 접근 (GET)
| 기능          | 함수명       | URI                                                      |
| --------    |  --------  | --------                                                  |
| 목록          | list       | GET    /samples/list.html?pathParameters...=...n...=...  |
| 상세          | detail     | GET    /samples/detail.html/{샘플아이디}                    | 
| 등록          | add        | GET    /samples/add.html                                 |
| 수정          | modify     | GET    /samples/modify.html/{샘플아이디}                    | 

### 데이터 조작 (FORM SUBMIT)
| 기능        | 함수명       | URI                                |
|  --------  |  --------  | --------                           |
| 등록        | post       | POST /samples/post                 |
| 수정        | modify     | POST /samples/modify               |
| 삭제        | delete     | POST /samples/delete               |

### 데이터 조작 (AJAX)
| 기능        | 함수명       | URI                                |
|  --------  |  --------  | --------                           |
| 조회        | schools    | GET /samples/schools               |
| 등록        | schools    | POST /samples/schools/{id}         |
| 수정        | schools    | PUT /samples/schools/{id}          |
| 삭제        | schools    | DELTE /samples/schools/{id}        |


* 페이지 : 호출 구분에 .html로 구분하여 호출하고 GET 호출 시 Path variable 유형 사용금지.
* 목록 : 복수로 작성
* 조건 목록 : 복수 뒤에 likes 를 붙이고 이하로 조건들을 작성
* 관계 목록 : 샘플의 댓글목록을 가져오는 것이다. 이 처럼 도메인의 구조에 맞춰 데이터를 가져올 경우 이와 같이 상하 구조대로 api를 디자인 한다.
* 등록 : 복수로 작성
* 수정 : 복수 뒤에 아이디
* 삭제 : 복수 뒤에 아이디
* 존재 여부 : 복수 뒤에 아이디

---
## 공통 자바스크립트 컴포넌트
* 공통 자바스크립트 컴포넌트는 Presentation 라이브러리를 사용을 보다 편리하게 하고, 공통영역으로 관리하기 위해 제공함.

### 1. jquery-ajax.js - [Jquery.com](https://jquery.com/)
* Jquery AJAX 기능을 보다 간결하고 공통적 요소로 사용하기 위해 제공.
    * 제공 함수
        1. JqueryAjax.FN.makeRequestUri()
           > 현재 페이지의 주소와 Path 파라미터를 가공하여 Map으로 구성한다. (서버에 보내기 위해 사용)
        2. JqueryAjax.DataAction.fnAjax( ... )
           > AJAX 데이터 통신을 위해 사용
            * 파라미터 설명

           ```javascript
           /**
            @sendURL: 요청주소입력 (String),
            @datavalue: 요청할 파라미터 (Array{}),
            @otherOptions: jquery ajax 의 다른 옵션을 변경할 때 사용 (Array{}),
            @resultFn: 처리 후 수행할 함수 (function()),
            @resultval: 처리 후 수행할 함수에 전달하는 값 (서버 처리와 상관관계없음),
            @forceAbortPR: 현재 요청이 있을 경우 강제 종료할 것인가에 대한 여부 (boolean)
           */                
           ```
            * 사용 예
            ```javascript
                JqueryAjax.DataAction.fnAjax({
                        sendURL: "/sample/samples",
                        dataValue: null,
                        otherOptions: null,
                        resultFunction: function(result) {
                            fn.createListTemplate(result);
                        }
                });
            ```
### 2. CKEditorHandler.js - [ckeditor.com](https://ckeditor.com/ckeditor-4/)
* WYSIWYG(위지윅) 에디터인 CKEditor 를 쉽게 사용할 수 있도록 제공.
    * 제공 함수
        1. CKEditorHandler.FN.create(textareaId)
           > 에디터의 생성
            * 사용 예
            ```html
            <textarea id="contents"></textarea> <!-- HTML ELEMENT -->
            ```

            ```javascript
            CKEditorHandler.FN.create('contents'); // JAVA SCRIPT
            ```

### 3. DatatablesHandler.js - [datatables.net](https://datatables.net/)
* AJAX 데이터 그리드 테이블을 쉽게 사용할 수 있도록 제공
    * 제공 함수
        1. DatatablesHandler.FN.create(tableid, options)
           > 데이터 테이블 생성과 비동기 데이터 조회
            * 사용 예
            ```html
            <!-- 데이터테이블 (그리드) 영역 -->
            <div class="container-fluid">
                <table id="myTable" class="table table-striped table-hover nowrap">
                    <thead>
                        <tr>
                            <th>School ID</th>
                            <th>Creator</th>
                            <th>Name</th>
                            <th>Name(ReadOnlyMode)</th>
                        </tr>
                    </thead>
                </table>
            </div>
            ```

            ```javascript
            DatatablesHandler.FN.create('myTable', {
                ajax: {
                    "url": listDataUrl
                },
                columns: [
                    { id:"1", data: "id",       className: "text-center" },
                    { id:"2", data: "creator",  className: "text-left", render:function(value, option, data) {
                        return '<a href="/sample/detail/' + data.id + '">' + value + '</a>';
                    }},
                    { id:"3", data: "name",     className: "text-center" },
                    { id:"4", data: "name",     className: "text-center", render:function(value, option, data) {
                        return '<a href="/sample/detail/' + data.id + '?readonly=true">' + value + '</a>';
                    }}
                ]
            });
            ```

        2. DatatablesHandler.FN.search(dataTable, url, params)
           > 데이터 조회
### 4. jquery-toastr.js - [toastr](https://github.com/CodeSeven/toastr)
* 깔끔한 토스트 형식의 안내 멘트 제공을 위하여 제공
    * 제공 함수
        1. JqueryToastr.FN.show(title, msg, options)
           > 메세지 표시(기본값: 상단 가운데)
            * 사용 예
            ```javascript
            JqueryToastr.FN.show("안내", "[저장]하였습니다.");
            ```

----
# 공통 페이징 INCLUDE (MVC 패턴)
* 일반적인 목록형 페이지에서 공통으로 사용할 페이징 컴포넌트가 마련되어 있으며 아래와 같이 사용할 수 있다.
* Thymeleaf 로 구성되어 있으며, th:replace를 설명한다.
    ```html
    <div th:replace="/fragments/pager :: frag(page=${page},url='/sample/list.html')">.... 페이징 영역 ....</div>
    ```
    1. th:replace 의 :: 를 기준으로 **["/fragments/pager"]** 는 "가져올 파일의 주소", 뒤이어 **frag("page..", "url...")** 이 있다.
    2. frag()의 설명
        * page: 현재 위치한 페이지를 나타낸다.
        * url: 데이터를 조회할 URL View 를 지정한다. 지금 페이지가 예를 들어 **list.html** 이라면 해당 주소를 적는다.
* Datatables를 사용할 경우 위의 과정은 필요없다..

---


